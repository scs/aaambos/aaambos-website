module.exports = {
    docs: [
        {
            type: 'category',
            collapsible: false,
            collapsed: false,
            label: 'Getting Started',
            link: {
                type: 'generated-index',
                title: 'Getting Started',
                description: 'The starting point to get to know AAAMBOS!',
                slug: '/category/aaambos-getting-started',
                keywords: ['getting-started'],
            },
            items: [
                'what_is_aaambos',
                'installation',
                'quickstart',
            ],
        },
        {
            type: 'category',
            label: 'Foundation',
            collapsible: false,
            collapsed: false,
            link: {
                type: 'generated-index',
                title: 'AAAMBOS Foundation',
                description: 'Learn about the most important AAAMBOS concepts!',
                slug: '/category/aaambos-foundation',
                keywords: ['foundation'],
            },
            items: [
                'terminology',
                {
                    type: 'category',
                    label: 'CLI Commands',
                    link: {
                        type: 'generated-index',
                        title: 'CLI Commands',
                        description: 'The CLI commands that you can use with AAAMBOS!',
                        slug: '/category/cli-commands',
                        keywords: ['commands', 'cli'],
                    },
                    items: [
                        'run',
                        'create',
                        'set',
                        'list',
                    ],
                },
                {
                    type: 'category',
                    label: 'Modules',
                    link: {
                        type: 'doc',
                        id: 'modules/index'
                    },
                    items: [
                        'modules/module_config',
                        'modules/module_info',
                        'modules/module_class',
                    ],
                },

                'features',
                'communication',
                'extensions',
                {
                    type: 'category',
                    label: 'Configs',
                    items: [
                        `hyperyml_configs`,
                        'run_config',
                        'arch_config',
                        'global_config',
                    ],
                },
            ],
        },
        {
            type: 'category',
            label: 'Package Pool',
            collapsible: false,
            collapsed: false,
            items: [
                {
                    type: 'category',
                    label: 'Std Library',
                    link: {
                        type: 'doc',
                        id: 'pkgs/std/index'
                    },
                    items: [
                        'pkgs/std/extensions',
                        'pkgs/std/guis',
                        'pkgs/std/modules',
                        'pkgs/std/supervision',
                    ],
                },
                {
                    type: 'category',
                    label: 'Dialog',
                    link: {
                        type: 'doc',
                        id: 'pkgs/dialog/index'
                    },
                    items: [
                        {
                            type: 'category',
                            label: 'Simple Flexdiam',
                            link: {
                                type: 'doc',
                                id: 'pkgs/dialog/simple_flexdiam/index'
                            },
                            items: [
                                'pkgs/dialog/simple_flexdiam/tiers',
                                'pkgs/dialog/simple_flexdiam/observers',
                                'pkgs/dialog/simple_flexdiam/flow_handler',
                                'pkgs/dialog/simple_flexdiam/issues',
                                'pkgs/dialog/simple_flexdiam/xmits',
                                'pkgs/dialog/simple_flexdiam/nlg',
                            ],
                        },
                        'pkgs/dialog/vosk_asr',
                        'pkgs/dialog/rasa_nlu',
                        'pkgs/dialog/basic_tts',
                    ],
                },
            ],
        },
        'faq',
        {
            type: 'link',
            label: 'API', // The link label
            href: 'https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos.html', // The external URL
        },
    ],
};

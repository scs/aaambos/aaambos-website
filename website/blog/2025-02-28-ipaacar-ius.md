---
slug: ipaacar-ius
title: Working with ipaacaR IUs in AAAMBOS
authors: fschroeder
tags: [ipaaca, ipaacar, ius, incremental, message passing, rust]
---

## What are IUs? What is ipaaca(R)?

A simple introduction to the original IPAACA gives this [wiki entry](https://scs.techfak.uni-bielefeld.de/wiki/public/ipaaca/start).
In short, two papers build the theoretical background for the foundation of IPAACA [[1]](http://www.aclweb.org/anthology/E/E09/E09-1081.pdf), [[2]](https://doi.org/10.5087/dad.2011.105).
The idea is to have a structure that represent the incremental structure of communication between modules.

> IPAACA is a framework for incremental processing via "incremental units" (IUs) processed by buffers (in and out). 
It uses a MQTT broker for message passing. Therefore, a MQTT broker must be installed and running to use ipaaca(r) (mosquitto, nanomq). 
Simple messaging is possible via "messages".
IUs can be updated, linked, committed, (and retracted,) allowing incremental processing, e.g. for conversational agents.

`ipaacaR` is a new implementation in Rust with Python bindings ([code](https://gitlab.ub.uni-bielefeld.de/scs/ipaacar), [Python docs](https://scs.pages.ub.uni-bielefeld.de/ipaacar/ipaacar-python/ipaacar.html)). 
The [`ipaacar_com_service`](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_ipaacar_com_service) is an AAAMBOS communication service that uses ipaacar for communication. It allows standard message passing and the usage of IUs.

### So, what are IUs?
_Incremental Units_ (IUs) are designed to be sent from modules. While messages are just _Fire-and-Forget_, on the other hand, IUs stay longer alive. 
They work best if only the owner module updates them. The advantage is that the data / information can be updated or better the status of the data, e.g., stay valid, etc.
Similar to messages, an IU is sent over a specific **topic** (=category).
They can be **linked** among themselves to establish where data comes from, e.g., linking several Text-To-Speech results that are consecutive or connect Natural-Language-Understanding results with the TTS results.

Other modules can receive new IUs and register notification for updates. Updating IUs that are not owned by the module should not be the standard usage, because they are (currently) very ineffective (~40ms vs ~1ms).
So, the general intended behavior is to send/update own IUs from each module when they have to say something.

## IUs with AAAMBOS
:::tip

You need the `ipaacar_com_serive` and a broker installed. Further the broker needs to run. Have a look at the [installation page](/docs/).

:::

A simple example is the [`PingPongIUModule`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/modules/ping_pong_example_iu.html).

### Define communication
To make sure that the communication is done with a communication service that can do IUs (e.g., ipaacar com service) we need to represent the requirement in the communication unit / promise.
Here, we define the `CountingMsg` for the ping pong example. 

```python
COUNTING = "Counting"
"""The topic used for the counting."""

CountingPromise, CountingUnit = create_promise_and_unit(
    name=COUNTING,
    payload_wrapper=CountingMsg,
    unit_description="Msg that contains increased value.",
# highlight-next-line
    required_com_attr=to_attribute_dict([IncrementalUnitTopicWrapperAttribute])
)
"""The promise and unit for the counting"""

CountingFeature = CommunicationFeature(
    name="CountingFeature",
    version=Version("0.0.1"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.0.1"),
    required_input=[CountingPromise],
    provided_output=[CountingPromise],
)
"""The Com feature for the counting. Both require and provide the count."""
```
The `CountingFeature` needs to be added to the `provide_features` method.

In the module, you can register a callback for IUs (for example in the `initialize` method)
```python
await self.com.register_callback_iu_for_promise(self.prm[COUNTING], self.handle_increased)
```

A IU can be created and send via the method `create_and_send_iu`:
```python
self.iu_own = await self.com.create_and_send_iu(self.tpc[COUNTING], CountingMsg(self.counter, self.name))
```

The `handle_increased` method is called when IUs of the `Counting` topic are created or updated.

It has this function header:
```python
async def handle_increased(self, topic: Topic, msg: CountingMsg, event: IUEvent, iu: 'IU', is_own: bool):
    ...
```
The `IUEvent` is an enum over `NEW`, `UPDATED`, and `COMMITED`. The `msg` argument is already in the msgspec `Struct`. 
The on the `iu` object can be called the [IU](https://gitlab.ub.uni-bielefeld.de/scs/ipaacar/-/blob/main/ipaacar-python/python/ipaacar/components.pyi?ref_type=heads#L4) methods (often coroutines.).

Again, the module should not update/set the payload of IUs that are not `is_own` for performance reasons. 
For example, the PingPong example uses two IUs. The pong module creates a new IU when the ping IU arrives the first time.
After that, the modules just update their IUs which informs the other module.
```python title="async def handle_increased(...):"
    if self.iu_own and is_own:
        return 
    self.counter = msg.value
    self.counter += 1
    self.log.info(f"{self.name}: {self.counter}")
    if self.iu_own:
        await self.com.set_payload_iu(self.iu_own, CountingMsg(self.counter, self.name))
    else:
        self.iu_own = await self.com.create_and_send_iu(self.tpc[Counting], CountingMsg(self.counter, self.name))
```

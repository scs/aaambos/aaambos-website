---
slug: python-libs-programming
title: Python Libraries for Programming in Python
authors: fschroeder
tags: [python, python3, libraries, recommendations, awesome-libraries]
---


- pendulum
- msgspec

- peewee
- sqlalchemy

import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';
import ThemedImage from '@theme/ThemedImage';

const features = [
    {
        title: 'Supervision',
        imageUrl: 'img/eye-scan-svgrepo-com(2).svg',
        imageUrlDark: 'img/eye-scan-svgrepo-com(2)-d.svg',
        description: (
            <>
                AAAMBOS allows you to start and stop modules, view their state and last status, and provides insight
                into their execution.
            </>
        ),
    },
    {
        title: 'Configuration',
        imageUrl: 'img/configuration-vertical-options-svgrepo-com.svg',
        imageUrlDark: 'img/configuration-vertical-options-svgrepo-com-d.svg',
        description: (
            <>
                The <code>run</code> and <code>arch</code>(itecture) configs allow you to configure your agent, the
                internal modules, and communications easily and quickly.
                An <code>arch config</code> provides an easy to share format of the architecture, while the <code>run
                config</code> can contain agent specific and sensitive information such as agent name or keys, pwds,
                etc.
                With HyperPyYaml, AAAMBOS can reference Python classes, modules, and functions in an easy and friendly
                way.
            </>
        ),
    },
    {
        title: 'Feature System',
        imageUrl: 'img/toggle-on-svgrepo-com.svg',
        imageUrlDark: 'img/toggle-on-svgrepo-com-d.svg',
        description: (
            <>
                Extend or customize modules by providing and requesting <b>features</b>. Features can be enabled or
                disabled according to the architectural needs/requirements.
                Features are used to express the need for extensions, communications and kwargs.
            </>
        ),
    },
    {
        title: 'Module Communication',
        imageUrl: 'img/connection-gateway-svgrepo-com.svg',
        imageUrlDark: 'img/connection-gateway-svgrepo-com-d.svg',
        description: (
            <>
                The basis for communication between AAAMBOS modules can be easily replaced or combined with different
                backends, due to the abstraction to communication services.
                Communication promises and units define the communication capabilities. Data structures (wrappers)
                further organize the exchange.
            </>
        ),
    },
    {
        title: 'Module Extensions',
        imageUrl: 'img/extension-svgrepo-com.svg',
        imageUrlDark: 'img/extension-svgrepo-com-d.svg',
        description: (
            <>
                Reusable module capabilities can be encapsulated using module extensions. These are easily added and
                accessed.
            </>
        ),
    },
    {
        title: 'Command Line Interface',
        imageUrl: 'img/terminal-access-pc-computer-screen-svgrepo-com.svg',
        imageUrlDark: 'img/terminal-access-pc-computer-screen-svgrepo-com-d.svg',
        description: (
            <>
                AAAMBOS provides command line tools. <i>Run</i>, <i>Create</i>, Test, Discover and even more are
                planned.
            </>
        ),
    },
    {
        title: 'Conventions',
        imageUrl: 'img/bank-svgrepo-com.svg',
        imageUrlDark: 'img/bank-svgrepo-com-d.svg',
        description: (
            <>
                <b>WIP</b> Define your architectural ecosystem with <i>conventions</i>, e.g., for communication and
                configuration.
                Customize AAAMBOS by defining your own system, changing the internal behaviour completely or just
                extending the CLI commands.
            </>
        ),
    },
    {
        title: 'GUIs for Agent Insights',
        imageUrl: 'img/configuration-control-options-svgrepo-com.svg',
        imageUrlDark: 'img/configuration-control-options-svgrepo-com-d.svg',
        description: (
            <>
                Create simple GUIs for each module or monitor and interact with communication data from a central point.
                Define visualizations for data in text, image, plot or other formats.
                Currently, GUIs in AAAMBOS are based on the easy-to-use and comprehensive PySimpleGUI library.
            </>
        ),
    },
    {
        title: 'Extensible & Reusable',
        imageUrl: 'img/package-svgrepo-com.svg',
        imageUrlDark: 'img/package-svgrepo-com-d.svg',
        description: (
            <>
                Create your own architecture by creating and reusing AAAMBOS pkgs. They are easily referenced in the
                HyperPyYaml configs.
                The AAAMBOS comes with the std library which already contains a number of useful modules, extensions,
                and conventions.
            </>
        ),
    },
    {
        title: 'Docs',
        imageUrl: 'img/magnifier-svgrepo-com(1).svg',
        imageUrlDark: 'img/magnifier-svgrepo-com(1)-d.svg',
        description: (
            <>
                AAAMBOS comes with different levels of documentation and learning content. At the top-level are blog
                posts with tutorials, recommendations, and examples. The next level is the "full-text" docs which
                contain examples and general usage instructions.
                Furthermore, the API documentation of AAAMBOS (core+std) and each AAAMBOS pkg provide specific and
                code-oriented documentation. Each AAAMBOS pkg comes with automatically generated API docs with pdoc per
                default. The blog and full-text docs are based on a docusaurus website.
            </>
        ),
    },
    {
        title: 'Examples',
        imageUrl: 'img/files-svgrepo-com.svg',
        imageUrlDark: 'img/files-svgrepo-com-d.svg',
        description: (
            <>
                We believe that learning by example is the easiest way to get started with a framework. AAAMBOS provides
                several architecture examples, and each pkg of a certain size should come with example configs that
                allow the new functionality of the pkg to be run individually.
            </>
        ),
    },
    {
        title: 'Testing',
        imageUrl: 'img/configuration-gears-svgrepo-com.svg',
        imageUrlDark: 'img/configuration-gears-svgrepo-com-d.svg',
        description: (
            <>
                <b>WIP</b> AAAMBOS and AAAMBOS pkgs come with automated unit and integration tests. Libraries such as
                pytest, hypothesis, ruff, etc. can help not only to find bugs, but also to improve code quality and
                standards.
            </>
        ),
    },
    {
        title: 'Growing Pkg Pool',
        imageUrl: 'img/warehouse-inventory-stock-merchandise-svgrepo-com.svg',
        imageUrlDark: 'img/warehouse-inventory-stock-merchandise-svgrepo-com-d.svg',
        description: (
            <>
                AAAMBOS has a growing ecosystem. New pkgs with modules, extensions and communication services make it
                easier to build complex architectures. Have a look at the pkg overview (WIP).
            </>
        ),
    },
    {
        title: 'Different Execution Levels',
        imageUrl: 'img/processor-svgrepo-com.svg',
        imageUrlDark: 'img/processor-svgrepo-com-d.svg',
        description: (
            <>
                <b>WIP</b> Currently, modules are executed in single processes. The plan is to provide the option to run
                modules only in different threads, or only asynchronously (asyncio).
            </>
        ),
    },
    {
        title: 'Future Plans',
        imageUrl: 'img/rocket-2-svgrepo-com(1).svg',
        imageUrlDark: 'img/rocket-2-svgrepo-com(1)-d.svg',
        description: (
            <>
                AAAMBOS is still not where we want it to be. Future plans include, besides the ones already listed
                above, better dependency resolution for feature requests, searching for modules with needed features in
                the installed libraries (via entry points) or online, setting and enabling features in the config, OS
                independence testing, more communication services, implementation in a faster language (Rust),
            </>
        ),
    },
];

function Feature({imageUrl, imageUrlDark, title, description}) {
    const imgURL = useBaseUrl(imageUrl);
    const imgURLDark = useBaseUrl(imageUrlDark);
    return (
        <div className={clsx('col col--4', styles.feature)}>
            <div className="text--center">
                <ThemedImage className={styles.featureImage} alt="Docusaurus themed image"
                             sources={{light: imgURL, dark: imgURLDark}}/>
            </div>
            <h3>{title}</h3>
            <p>{description}</p>
        </div>
    );
}

export default function Home() {
    const context = useDocusaurusContext();
    const {siteConfig = {}} = context;
    return (
        <Layout
            title={`${siteConfig.title}`}
            description="Main Page of AAAMBOS: Architectures for Autonomous Agents: A Modular Basis with an Operating Supervisor">
            <header className={clsx('hero hero--primary', styles.heroBanner)}>
                <div className="container">
                    <h1 className="hero__title">{siteConfig.title}</h1>
                    <p className="hero__subtitle">{siteConfig.tagline}</p>
                    <div className={styles.buttons}>
                        <Link
                            className={clsx(
                                'button button--outline button--lg',
                                styles.getStarted,
                            )}
                            to={useBaseUrl('docs/')}>
                            Get Started
                        </Link>
                    </div>
                </div>
            </header>
            <main>
                {features && features.length > 0 && (
                    <section className={styles.features}>
                        <div className="container">
                            <div className="row">
                                {features.map((props, idx) => (
                                    <Feature key={idx} {...props} />
                                ))}
                            </div>
                        </div>
                    </section>
                )}
            </main>
        </Layout>
    );
}

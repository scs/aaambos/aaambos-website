/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'AAAMBOS Framework',
  tagline: 'Architectures for Autonomous Agents: A Modular Basis with an Operating Supervisor',
  url: 'https://scs.pages.ub.uni-bielefeld.de/',
  baseUrl: '/aaambos/aaambos-website/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: '/img/icons8-anvil-50.ico',
  organizationName: 'Florian Schröder', // Usually your GitHub org/user name.
  projectName: 'AAAMBOS', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'AAAMBOS',
      logo: {
        alt: 'aaambos logo',
        src: 'img/Ambos_4.svg',
        srcDark: 'img/Ambos_4_Dark.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.ub.uni-bielefeld.de/scs/aaambos',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              to: 'docs/category/aaambos-getting-started',
            },
            {
              label: 'Installation',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Stack Overflow',
              href: 'https://stackoverflow.com/questions/tagged/aaambos',
            },
            {
              label: 'SCS Website',
              href: 'https://scs.techfak.uni-bielefeld.de/',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/ag_scs',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.ub.uni-bielefeld.de/scs/aaambos',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Florian Schröder. Built with Docusaurus.`,
    },
    docs: {
      sidebar: {
        hideable: true,
      },
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos-website/-/tree/main/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos-website/-/tree/main/website/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  plugins: ['docusaurus-lunr-search'],
  markdown: {
    mermaid: true,
  },
  themes: ['@docusaurus/theme-mermaid'],
};

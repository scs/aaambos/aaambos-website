---
slug: install-aaambos
title: Install AAAMBOS
authors: fschroeder
tags: [installation, aaambos, getting-started]
---

:::info
There is now a dedicated **[Installation Page](/docs/)** for `aaambos` in the **Docs**.
:::

## How to install AAAMBOS on your machine?

_First of all you need:_
- (recommended) a Linux operating system, e.g., Ubuntu. The guide will use the Linux/Ubuntu shell commands.
- `conda`: You can install [miniconda](https://docs.conda.io/projects/miniconda/en/latest/) (recommended) or [Anaconda](https://docs.anaconda.com/free/anaconda/install/index.html) following these guides. 

:::tip

For python programming, debugging and execution use an IDE like [PyCharm](https://www.jetbrains.com/pycharm/).

:::

If you do a lot with conda, consider install [mamba](https://mamba.readthedocs.io/en/latest/mamba-installation.html#mamba-install). It's much faster!

The installation process:
- Create a new conda environment `aaambos` with Python 3.10:

```bash
conda create -n aaambos python=3.10
```
- Activate the environment:

```bash
conda activate aaambos
```
:::note

You can deactivate the environment with `conda deactivate`.

:::

- Install `aaambos` via pip.

```bash
pip install aaambos@git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos@main
```
- You probably need also a communication service. At the moment, the ipaacar com is the only available. Install it with

```bash
pip install ipaacar_com_service@git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_ipaacar_com_service.git
```

:::caution
For ipaacar communication you need a running mqtt broker when running an aaambos architecture. You can use **nanomq** if you not already have one running (e.g. Mosquitto). Apt-get installation of nanaomq [here](https://nanomq.io/downloads?os=Linux). You need to start the broker before starting any architecture with  `nanomq start` inside a shell/terminal. Just keep the shell open.
:::

:::caution
It looks like that the referenced ipaacar wheel (via a dependency in the `setup.py` in the ipaacar communication service) only runs on Ubuntu 22.04. You can build ipaacar from scratch ([repo](https://gitlab.ub.uni-bielefeld.de/scs/ipaacar), following the instructions in the `README.md`). After that and the installation of the created wheel in the `aaambos` conda environment, you need to install the aaambos_pkg_ipaacar_com_service without dependencies `pip install ipaacar_com_service@git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_ipaacar_com_service.git --no-deps`.
:::

That's it for the installation part of `aaambos`!

Read more in the Introduction part.


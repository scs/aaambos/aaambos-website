---
title: Terminology
---

## General Terms

### Module
The components of the architecture. They live individually in own processes. The framework provides a base classes to write these [Modules](/docs/modules).

### Extension
Common/Repeated functionality can be grouped in an [Extension](/docs/extensions) that can be added to several modules. E.g., ModuleStatus handling, etc.

### Feature
An abstract capsule of a requirement on modules. A Module can provide and request features. See [Features](/docs/features)

### Communication
The exchange of messages/data between modules in an architecture. More terminology is described [here](#communication-terms). An introduction to communication in _aaambos_ can be found [here](/docs/communication). 

### Supervision
The setup, starting and managing of modules in the architecture. More terms: [supervision terminology](#supervision-terms).

### Setup
The solving and decision of features, extensions, and modules before the agent/architecture is started with its modules.

### Package / pkg
A repository that contains code with a common topic. This repository is easy to install and to integrate in other architectures due to its installation as a python library. You can find information how to create a pkg [here](/docs/create#create-pkg)

### Config
On one hand, configs are the files that specify the configuration of an agent/architecture. Here we mostly use yaml/[HyperPyYaml](/docs/hyperyml_configs). 
But specific parts/classes use only parts of the configs. For example, the module config is only a subset of the [arch_config](/docs/arch_config). 
On the other hand, the layout/structure of a config and subparts is described via `attrs` classes (similar to dataclasses). These classes are also called configs.
The file configs are parsed and converted to instances of these `attrs` classes. Therefore, these classes often contain default values that are not visible in the yaml files.

### Logger
_aaambos_ passes to the module classes a logger attribute. At the moment, it is an instance of a [loguru](https://loguru.readthedocs.io/en/stable/) logger. The logs are written to a log file, in the folder of the agent.
Currently, these files do not cover additional stdout or stderr output. The `log` attribute of a module is the looger instance.

## Communication Terms

### Service
The instances of a [communication service](/docs/communication#communication-service) are part of each module. It handles the receiving and sending of messages for the module.
It can be accessed via `self.com` in a module and is used in the most cases to register callbacks and send messages.

_aaambos_ is designed to provide different types of communications services (e.g., backends). At the moment, only one for IPAACAR and one for Redis is implemented.

### Promise
A [communication promise](/docs/communication#units-and-promises) contains the information from a module what messages (or similar) it plans to send. It contains a reference to the unit with further info but also its idea is to store the information about frequency etc.

The promise is used on both sides the sender and the receiver modules.

A module needs the promises to easily register a callback. It can access the promises by the `self.prm` dict. The keys are the leaf topic. 

### Unit
The [communication unit](/docs/communication#units-and-promises) is part of the communication promise. It contains the wrapper class (the structure of the message).

### Topic
On [topics](/docs/communication#topics), the service publishes message. Other words are category or channel. The name of the topic is set on runtime (useful for multi-agent systems).
Therefore, _aaambos_ uses Hierarchical topics. The leaf topic is designed to be known by the module beforehand, so they can reference them in their code.

The topic is relevant for sending messages. A module can access them via the `self.tpc` dict. The leaf topics are the keys of the dict.

### IU
A more complex message to send. Its full name is _incremental unit_. Compared to messages, IUs can updated after they are send, which will trigger an update call in other modules on the IUs.
Further, IUs can link to other IUs.

### Message / Msg
The simple and fast method to send information from one module to another. It contains a payload and is sent via a topic. Its payload is often defined by a data wrapper class.

### Mediator
In the future, a module may use different communication services. E.g., for some topics service A and for other service B.
The mediator has the task to still provide the module with only one service and the mediator does the coordination part.

### Wrapper
The structure of the payload is defined in a wrapper. Or different: The dict data is wrapped in a class for better usage inside the modules (access by class attribute instead of arbitrary dict access).

At the moment, _aaambos_ uses [msgspec.Struct](https://github.com/jcrist/msgspec)s because of its speed. Sadly, with PyCharm it does not provide auto suggestions for keyword arguments and the base class docs of the struct classes are kind of heavy (therefore you should always add a small doc string to a Struct class).

## Module Terms

### ModuleInfo
Before all Module start, the architecture selects all information about the modules for the setup. 
The [ModuleInfo](/docs/modules/module_info) is the base class for providing the relevant information via overriding/implementing methods.
The outsourcing from the main module class allows the import of the module only during its execution and in its process.
If this is not wanted, you can still implement all functionality in one class, just by inherit from two classes.

### ModuleConfig
The `attrs` class that defines the structure of the [module config](/docs/modules/module_config). The base class already defines some relevant attributes.

## Supervision Terms
These terms are most likely relevant to you if you want to deep dive into _aaambos_.

### Init Preparer
The Init Prepares is created first. It loads the [run_config](/docs/run_config), checks if it defines a different run_config class and then converts the run_config dict to the run_config class.
If the agent is configured to have a `always_unique_instance` name, the init prepares sets the agent name.
It loads and converts the arch_config. Based on the run- and arch_config the preparer extracts the supervisor class and starts it.

### Arch Supervisor
The ArchSupervisor is running for the lifetime of the agent. It starts the architecture setup, creates relevant directories for the agent, setups the module configs, initializes the setup of the features and communcation services. It initializes the Run Time Manager and the Module Runner. Further, it calls the module runner step function.

### Arch Setup
The ArchSetup contains the logic to merge the module configs, solve the features, and communications services.

### Module Runner
The module runner calls the Run Time Manager step function. But mainly, it starts the module processes and overwatches the process states.
It requests the Run Time Manager what to do if a process dies. It can also stop and restart modules based on the instructions from the Run Time Manager,

### Run Time Manager
The RunTimeManager decides what to do with each module if the process stops. Or in general when to stop a module.
The default behavior is to restart modules if their process died with an error (termination code != 0) and not restart a process if it terminates with the termination code 0. 
Each module individually, can define in its ModuleConfig if it wants to be restarted in general.
The [InstructionRunTimeManager](/docs/pkgs/std/supervision#instruction-run-time-manager) allows instructing the run time manager via communication messages. Via the [Module Status Manager](/docs/pkgs/std/modules#module-status-manager) which contains a GUI, it can be controlled directly.
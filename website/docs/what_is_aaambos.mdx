---
title: What is AAAMBOS?
---
import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';

<p align="center">
<ThemedImage
  alt="AAAMBOS Logo"
  width="40%"
  sources={{
    light: useBaseUrl('/img/Ambos_4.svg'),
    dark: useBaseUrl('/img/Ambos_4_Dark.svg'),
  }}
/>
</p>

AAAMBOS, A³MBOS, A³mbos, _aaambos_, and a³mbos stands for
> **A**rchitectures for **A**utonomous **A**gents: A **M**odular **B**asis with an **O**perating **S**upervisor

## Framework
It is a framework for developing, executing, and maintaining architectures for autonomous agents. 
These agents do not need to follow the same architecture. 
Moreover, AAAMBOS provides the **supervisor** to set up and operate the agents. 
The general structure of the architectures can be completely independent. 
AAAMBOS follows the assumption that an architecture consists of different **modules** that encapsulate different capabilities or implement cognitive, interactive, autonomous functions.
Therefore, AAAMBOS provides a basis for such modules. 
For compatibility and standardization, AAAMBOS comes with a **feature** system, where each module can provide features and can request/require features from an architecture.
So that for an architecture/agent, AAAMBOS can check if all feature needs are satisfied and what "capabilities" (if represented as features) the agent has before the agent is started.
The interchangeability of modules is also increased, because modules with the same features can easily replace each other.
The long-term goal of AAAMBOS is to build a module pool from that agent designer can choose from and researchers can easily build and reuse components for faster prototyping and advanced research.
Such library can extend the longevity of developed modules and systems. 
Which often fail to live longer than the researcher stay involved.
The lack of documentation is approached by AAAMBOS via the automated generation of API Docs which supports the creation of simple but nice looking documentation.
In the future, maintainer could put requirements on a AAAMBOS package like percentage of test coverage, documented classes and methods, etc.

<p align="center">
<ThemedImage
  alt="AAAMBOS Hierarchy Image"
  sources={{
    light: useBaseUrl('/img/aaambos_hierarchy_simple.svg'),
    dark: useBaseUrl('/img/aaambos_hierarchy_simple_dark.svg'),
  }}
/>
</p>

The need for such framework arose from the development of the socio-interactive architecture presented in
> Stange, S., Hassan, T., Schröder, F., Konkol, J., & Kopp, S. (2022). Self-explaining social robots: An explainable behavior generation architecture for human-robot interaction. Frontiers in Artificial Intelligence, 5, 87. [🔗](https://www.frontiersin.org/articles/10.3389/frai.2022.866920/full)

Therefore, the early modules in AAAMBOS will orientate on the modules presented there. 
Another motivation for the framework was the need for a simpler installation and sharing process of architectures implemented.
Architectures are defined in a single config file (`arch_config`). Execution dependent information like agent name or access keys and passwords are located in second file, the `run_config`.

Currently, AAAMBOS is implemented in Python which is the most used programming language in machine learning and AI.
Its low barriers to entry and the great library pool of algorithm is the best starting point for researcher to develop intelligent agents at the moment.
On the other side, the performance of Python could evolve to the bottleneck of greater and more competent artificial agents.
Even of there exists methods to boost the performance (transfer to other languages C, Rust), the decision for Python as the basis for AAAMBOS is not immovable.

## Supervisor
The supervisor of AAAMBOS consists of an architecture setup part and the module runner part that executes the architecture.
Set up the features from the configured modules including the communication and extension prerequisites is the done before the execution.
The supervision during execution is done by the runner part and the manager that can start terminated modules. At the moment, each module is executed in an own process, for maximum parallelism.
The plan is to develop thread-based parallelism and asynchronous (asyncio) execution of modules.
For a GUI-based management of the modules, there exists an additional module that can be added to your `arch_config`. 
It lets the supervisor terminate and restart modules by hand and visualizes the process state of the module and more detailed module statuses (if implemented in the modules). It also provides access to the logs outputted by the modules. 
The additional module is part of the _std_ library of AAAMBOS.

## Modular Basis
The base for a module is divided in three classes. 
The `Module` base class containing the general execution of the module. 
The `ModuleInfo` containing the information relevant for the architecture about the module, e.g., features.
And the `ModuleConfig` which is the wrapper/dataclass for the config of the module, defining the expected attributes/keys, types and default values for the configuration of the module.
The separation in different classes allows to import the `Module` during the real execution (in the individual process) and not during the setup which would be present for all modules later on.
For modules that require libraries with longer import times, e.g., tensorflow, pytorch, this can become handy.
Otherwise, the `Module` and `ModuleInfo` class can be inherited by the implemented module class due to the multi inheritance in Python.
Modules are more described on the [Modules](/docs/modules/index) page.

A very simple example module is the [`PingPongModule`](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos/-/blob/main/aaambos/std/modules/ping_pong_example.py?ref_type=heads). 
The auto-generated API Docs for the module are located [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/modules/ping_pong_example.html).
The `arch_config` that uses the module is also part of the [AAAMBOS library](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos/-/blob/main/examples/arch_config_ping_pong.yml?ref_type=heads)
A `run_config` is located [here](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos/-/blob/main/examples/run_config_pg.yml?ref_type=heads)

Same functionality in several modules can be encapsulated in extensions. 
They are added to the modules at run-time. See [Extensions](/docs/extensions/) for more information.

## Feature System
There are different kinds of features:
- Communication features
- Extension features
- and normal features

Features are provided by modules and modules require features with a necessity (optional or required).
Using the same features allows to replace modules. 
The features are described in detail [here](/docs/features/)

<p align="center">
<ThemedImage
  alt="AAAMBOS Features Image"
  width="50%"
  sources={{
    light: useBaseUrl('/img/aaambos_features.svg'),
    dark: useBaseUrl('/img/aaambos_features_dark.svg'),
  }}
/>
</p>

## Communication
The different modules need to communicate with each other. For this, AAAMBOS comes with a definition of communication service. 
Different message brokers can be replaced by providing the same attributes in the service. 

The incremental processing of input is the main objective of ipaaca. 
It provides also a way to send atomic messages. 
At the moment, it is the provided communication service by AAAMBOS. 
Under the hood it uses a MQTT Broker.
In the future, other approaches can be used to define communication services, like RabbitMQ, ZeroMQ, Redis Pub/Sub, etc.
The usage and implementation of the communication system is described on the [Communication](/docs/communication/) page.

An example of a more complex communication than between the `PingPongModule`s is the communication between the [`ModuleStatusManager`](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos/-/blob/main/aaambos/std/modules/module_status_manager.py?ref_type=heads) and the [`ModuleStatusExtension`](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos/-/blob/main/aaambos/std/extensions/module_status/module_status_extension.py?ref_type=heads)

## Main Target Domain: Autonomous Agents
Even if the framework is oriented around autonomous agents, we believe that structured approach to execute complex systems can be used for other use cases.


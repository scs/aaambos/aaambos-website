---
title: Create
---

The `create` command can create _aaambos_ pkgs, modules, extensions, configs and communication promises based on templates.

The directory location in which the generated files will be located can be set with the argument `-l` or `--location` otherwise it will be generated in the current directory.

The command is intended to support the start of the development of new pkgs, modules and extensions by providing the base with common files (in the pkg and config case) and classes and methods (modules, extensions, and com_promises)..

The API Docs of the `create` command can you find [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/commands/create.html).

## Create `pkg`
The command uses [cookiecutter](https://cookiecutter.readthedocs.io/en/stable/) to create an _aaambos_ pkg based on the [_aaambos_ cookiecutter template](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/cookiecutter-aaambos-pkg).
```bash
aaambos create pkg
```
The cookiecutter dialog will ask you how to fill some variables in the template.
It will try to generate most of the variables based on the previous answers. All of the information can be changed later. It is just easier to create it right away with the correct data.
The questions and variables are:

- _full_name_: Your **Full Name**,
- _ub_gitlab_username_: Your uni-bielefeld **Gitlab** username,
- _email_: Your **Email Address**,
- _website_: A URL to a **Website** to your profile or personal website,
- _aaambos_system_: Specify a **Subsystem** of aaambos or a vanilla package with _aaambos_,
- _project_name_: The human-readable version of the **Prokect Name** (is converted to lower case and underscores for the 'src' folder),
- _has_aaambos_modules_: Should the pkg include aaambos **modules**?,
- _has_aaambos_extensions_: Should the pkg include aaambos **extensions**?,
- _has_aaambos_com_: Should the pkg include a aaambos **communication service**?,
- _has_aaambos_configs_: Should the pkg include **configs**?,
- _has_aaambos_guis_: Should the pkg include aaambos **guis**?,
- _has_aaambos_convs_: Should the pkg include aaambos architecture **conventions**?,
- _is_aaambos_system_: Defines this pkg an aaambos **system**?,
- _repo_name_: The name of the repository (includes aaambos or subsystem reference),
- _licence_: Choose a license,
- _noarch_python_: Is the pkg OS independent (Win, Ub, Mac, x64, arm, etc.) (is it just python?),
- _include_cli_: Has it python entry points? (own \_\_main\_\_),
- _gitlab_parent_: The project structure in gitlab (just your username or project with subprojects),
- _pkg_home_web_: The resulting URL of the repo,
- _project_short_description_: A short description about the content of the pkg,
- _version_: The semantic version of the project,
- _year_from_: License start **year**,
- _year_to_: License end **year**,
- _use_pytest_: Do you want to use pytest?

:::info
With the `-v`. `--version`, you can specify a version (branch, tag, or similar). It is passed to cookiecutter as the `checkout` argument.

Currently, the `-v flexdiam_pkg` creates a pkg designed for a [simple flexdiam](/docs/pkgs/dialog/simple_flexdiam/) addon with issue, observer, and training data examples. 
:::

## Create `module`

```bash
aaambos create module -n MyNewModule
```
The `--name` or `-n` should be in CamelCase. The relevant parts are converted to snake_case and SCREAMING_SNAKE_CASE.

The generated file is based on the [module example file in the cookiecutter template repo](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/cookiecutter-aaambos-pkg/-/blob/main/%7B%7Bcookiecutter.repo_name%7D%7D/%7B%7Bcookiecutter.__package_name%7D%7D/modules/my_new_module.py?ref_type=heads).
It still contains the `{{ cookiecutter.__package_name }}` string which should be replaced with the project name, e.g., the parent folder of the `modules` folder.

## Create `extension`

```bash
aaambos create extension -n MyNewExtension
```
The `--name` or `-n` should be in CamelCase. The relevant parts are converted to snake_case and SCREAMING_SNAKE_CASE.

The generated file is based on the [extension example file in the cookiecutter template repo](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/cookiecutter-aaambos-pkg/-/blob/main/%7B%7Bcookiecutter.repo_name%7D%7D/%7B%7Bcookiecutter.__package_name%7D%7D/extensions/my_new_extension.py?ref_type=heads).

## Create `run_config`, `arch_config`, `configs`

The command copies either the `run_config`, `arch_config` or both from the [cookiecutter tempkate](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/cookiecutter-aaambos-pkg/-/tree/main/%7B%7Bcookiecutter.repo_name%7D%7D/%7B%7Bcookiecutter.__package_name%7D%7D/configs?ref_type=heads)

Copy the `run_config`:
```bash
aaambos create run_config
```
Copy the `arch_config`:
```bash
aaambos create arch_config
```
Copy the `run_config` and `arch_config`:
```bash
aaambos create configs
```
## Create `com_promise` or `promise`

This command prints code that you can copy into your files, e.g., module, extension or custom files.
The code defines a msgspec Struct, the communication Promise, Unit, and the ComFeatureIn and ComFeatureOut.
In the comments, it describes what features add to the `provides_features and `requires_features` methods in a module.

```bash
aaambos create promise -n MyNewMsg
```

The example above generates the following output:

```python showLineNumbers
from datetime import datetime
from msgspec import Struct, field as m_field
from aaambos.std.communications.utils import create_promise_and_unit, create_in_out_com_feature
from aaambos.std.communications.categories import MSGSPEC_STRUCT_TYPE            

MY_NEW_MSG = "MyNewMsg"


class MyNewMsg(Struct):
    """MyNewMsg datastructure"""
    # TODO add attributes of the msg
    time: datetime = m_field(default_factory=lambda: datetime.now())


MyNewMsgPromise, MyNewMsgUnit = create_promise_and_unit(
    name=MY_NEW_MSG,
    payload_wrapper=MyNewMsg,
    unit_description="__description of the content of the msg__",
    frequency=1,
    version_str="0.1.0",
    required_service_categories=[{MSGSPEC_STRUCT_TYPE}],
)

MyNewMsgComFeatureIn, MyNewMsgComFeatureOut = create_in_out_com_feature(
    name=MY_NEW_MSG, promise=MyNewMsgPromise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0"
)

# # add the features to your modules
# # modules that send the feature (provide out-feature (and require that some module provides the in-feature)):
# # inside the dict of the provides_features method:
# MyNewMsgComFeatureOut.name: (MyNewMsgComFeatureOut, SimpleFeatureNecessity.Required),
# # inside the dict of the requires_features method:
# MyNewMsgComFeatureIn.name: (MyNewMsgComFeatureIn, SimpleFeatureNecessity.Required)

# # modules that receive the feature (provide in-feature (and require that some module provides the out-feature)):
# # inside the dict of the provides_features method:
# MyNewMsgComFeatureIn.name: (MyNewMsgComFeatureIn, SimpleFeatureNecessity.Required)
# # inside the dict of the requires_features method:
# MyNewMsgComFeatureOut.name: (MyNewMsgComFeatureOut, SimpleFeatureNecessity.Required),
# # imports: from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
# # and the MyNewMsgComFeatureOut and MyNewMsgComFeatureIn based on their location.
```
:::info
If you want to add the content directly to a file, you can specify the file with `-f` or `--file` and the file will be appended with the content. 
If the file does not exist, it will be created.
:::

## Extending the create command in own _aaambos_ pkgs
You can extend the create command in an _aaambos_ pkg. This can be done via Python "entry points". An example usage can be found in the [Simple Flexdiam](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_simple_flexdiam/simple_flexdiam/plugins.html) pkg.

You need to add an `aaambos.core.create` entry point with a function that accepts the [Create](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/commands/create.html#Create) class as an argument.
```python titel="setup.py"
setup(
    ...,
    entry_points={
        'aaambos.core.create': [
            'what_to_create = {{pkg_name}}.plugins.create:your_create_func',
        ],
    },
)
```
Replace 
- `what_to_create` with the word for the positional argument in the `create` command (`aaambos create what_to_create`)
- `{{pkg_name}}` with the name of the pkg / lib (the import name)
- optional: the path to your func (`.plugins.create`)
- `your_create_func` with the name of your function

Your function header should look like
```python
from aaambos.core.commands.create import Create


def your_create_func(create: Create):
    ...
```

:::info
**For Example**:
- [Simple Flexdiam](/docs/pkgs/dialog/simple_flexdiam/#extended-create-command) provides create options for issues, observers, xmits, and tiers.
- [StreamlitGui](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_streamlit_gui/streamlit_gui.html) provides a create option for StreamlitGuiElements.
:::

:::note
An alias is `template`. It can be used in the same way as `create`.
:::
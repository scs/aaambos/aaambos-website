---
title: Run
---

The `run` command is the main execution part of _aaambos_. It starts an architecture / agent with a `run_config` and an `arch_config`.

```bash
aaambos run --run_config=my_package/configs/run_config.yml --arch_config=my_package/configs/arch_config_new.yml
```

You can add for every command (not just the `run` command) the argument `-ll` or `--log-level`. The default is `TRACE`. It is based on the [loguru log levels](https://loguru.readthedocs.io/en/stable/api/logger.html#levels).
Possible choices are `"TRACE"`, `"DEBUG"`, `"INFO"`, `"SUCCESS"`, `"WARNING"`, `"ERROR"`, `"CRITICAL"`. 
It sets the loglevel for the supervisor and architecture setup.

## Required arguments

### `run_config`
Provide the path to the `run_config` to use. 

For the allowed content of the `run_config` see [run_config](run_config.md).

### `arch_config`
Provide the path to the `arch_config` to use. 

For the allowed content of the `arch_config` see [arch_config](arch_config.md).

:::note
An alias is `start`. It can be used in the same way as `run`.
:::
---
title: The Arch Config
---

## Role
The `ArchConfig` defines the architecture/agent to run. 

## Parts

See [API Dccs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/configuration/arch_config.html#ArchConfig)

### Concurrency
Currently only `MultiProcessing` is implemented.

```yaml
concurrency: MultiProcessing
```

### Communication
Define the communication preferences.

```yaml
communication:
  communication_prefs:
   - !name:ipaacar_com_service.communications.ipaacar_com.IPAACARInfo
```

### Modules
Define the modules and their config.

```yaml title="Example for an agent with dialog abilities"
modules:
  vosk_asr:
    module_info: !name:vosk_asr.modules.asr_simulator.AsrSimulator
    mean_frequency_step: 10
    # module_info: !name:vosk_asr.modules.vosk_asr.VoskAsrModule
  rasa_nlu:
    module_info: !name:nlu_rasa.modules.rasa_nlu_module.RasaNluModule
  basic_tts:
    module_info: !name:basic_tts.modules.basic_tts.BasicTts
  simple_flexdiam:
    module_info: !name:simple_flexdiam.modules.simple_flexdiam_module.SimpleFlexdiam
    flow_handler:
      flow_handler_class: !name:simple_flexdiam.modules.issue_flow_handler.issue_flow_handler.IssueFlowHandler
      root_issue: !name:simple_flexdiam.modules.std.issues.root_issue.RootIssue
      shadow_root_issues:
        - !name:simple_flexdiam.modules.std.issues.greet_issue.GreetIssue
        - !name:simple_flexdiam.modules.std.issues.how_are_you_issue.HowAreYouIssue
        - !name:simple_flexdiam.modules.std.issues.repeat_issue.RepeatIssue
      entry_point_finder: !name:simple_flexdiam.modules.issue_flow_handler.simple_entry_point_finder.SimpleEntryPointFinder
    #  flow_handler_class: !name:simple_flexdiam.modules.std.dummy.dummy_flow_handler.DummyFlowHandler
  gui:
    module_info: !name:aaambos.std.guis.pysimplegui.pysimplegui_window.PySimpleGUIWindowModule
    window_title: Counting GUI
    topics_to_show: [["AsrResults", "simple_flexdiam.modules.core.definitions.AsrResults", ["best"]], ["NluResults", "simple_flexdiam.modules.core.definitions.NluResults", ["intention"]]]
  status_manager:
    module_info: !name:aaambos.std.modules.module_status_manager.ModuleStatusManager
```

### Description
A string that can describe the architecture/agent in natural language.

## Future Parts:
- Architecture Conventions
- Extension Configs
- Supervisor Config
- ...
---
title: Set
---

The `set` command lets you configure the [global aaambos config](/docs/global_config). 
As the first positional argument you specify the name of the option and as the second the value:
```bash
aaambos set local_agent_directories ~/my_own_aaambos_agents_dir
aaambos set always_unique_instance false
```

The default values are "None" and can be set back via `None` or `null`. Then, the global config does not overwrite the other configs.
At the moment only `local_agent_directories` and `always_unique_instance` are covered.

:::info
For values with the expected type `bool` are the following values (not case-sensitive) converted:
- `True`: y, yes, t, true, on, 1
- `False`: n, no, f, false, off, 0
:::

You can set the values to their default so that they do not change run or arch configs either by hand or via the `set` command and `reset`/`default`:
```bash
aaambos set reset all
```
Or single values:
```bash
aaambos set reset local_agent_directories
```
You can see the correct execution via `aaambos list global_config`. 

:::note
Aliases are `global` and `configure`. They can be used in the same way as `set`.
:::
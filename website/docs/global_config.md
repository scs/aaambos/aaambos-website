---
title: The  Global Config
---

The _aaambos_ global config stores persistently configuration for all executed agents/architectures. It overwrites values in the arch and run config.

At the moment, these options are implemented:
- `local_agent_directories`
- `always_unique_instance`

The location of the global config depends on your OS. _aaambos_ uses the [platformdirs](https://pypi.org/project/platformdirs/) pkg to locate the `user_config_dir`.
:::info
On Linux it is `~/.config/aaambos/`
:::

You can either manipulate the file by hand or use the _aaambos_ command [set](/docs/set).
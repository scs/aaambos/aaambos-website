---
title: Module Info
---

The [`ModuleInfo`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/module/base.html#ModuleInfo) class of a module contains the information relevant for the architecture setup.
Therefore, the required and provided features are defined, the module config class is referenced and the function that will start / set up the module.

The `ModuleInfo` class is the only attribute that needs to be referenced in the `arch_config.yml` for every module.

### _abstract_ _cls_ `provides_features`

Here the module can define which features are added by the module to the architecture.

**Example**

The [`ModuleStatusManager`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/modules/module_status_manager.html#ModuleStatusManager.provides_features) provides _CommunicationFeatures_:
```python title="module_status_manager.py" showLineNumbers {5-7}
@classmethod
def provides_features(cls, config: ModuleConfig = ...) -> dict[FeatureName, tuple[Feature, FeatureNecessity]]:
    """Provides: Input usage of ModuleStatusInfo and ModuleStates. Outputs ModuleManagerInstruction """
    return {
        ModuleStatusInfoComFeatureIn.name: (ModuleStatusInfoComFeatureIn, SimpleFeatureNecessity.Required),
        ModuleStatesComFeatureIn.name: (ModuleStatesComFeatureIn, SimpleFeatureNecessity.Required),
        ModuleManagerInstructionFeatureOut.name: (ModuleManagerInstructionFeatureOut, SimpleFeatureNecessity.Required)
    }
```

:::tip

For both `provides_features` and `requires_features`, the decorators `optional` and `required` introduce syntactic sugar (/make things easier).
You just need to add the features to the decorator and the decorator will add them to the output in the dictionary format of (`name: (feature, necessity)`).

```python title="Example from the ModuleStatusManager" 
@classmethod
@required(
    ModuleStatusInfoComFeatureOut,
    ModuleStatesComFeatureOut,
    ModuleManagerInstructionFeatureIn,
    RunConfigAccessFeature,
    ModuleStatusesComFeatureOut,
)
def requires_features(cls, config: ModuleStatusManagerConfig = ...) -> dict[FeatureName, tuple[Feature, FeatureNecessity]]:
    """Requires: ModuleStatusInfo, ModuleStates Outputs from other modules, ModuleManagerInstruction input. And the extensions: RunConfigAccess and the PySimpleGUIWindowExtension """
    features = {}
    if config.gui:
        features.update({PySimpleGUIWindowExtensionFeature.name: (PySimpleGUIWindowExtensionFeature, SimpleFeatureNecessity.Required)})
    return features
```

You can use them with the import [`from aaambos.core.module.feature import required, optional`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/module/feature.html#required).

:::

**Arguments**

| Name     | Type               | Details                                                                  |
|----------|--------------------|--------------------------------------------------------------------------|
| `config` | [`ModuleConfig`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/configuration/module_config.html#ModuleConfig) | the module config can provide information about the features to provide. |

**Return Value**

The different expected return types can have the following formats
- `dict[FeatureName, tuple[Feature, FeatureNecessity]]`
- `dict[str, tuple[Feature, FeatureNecessity]]`
- `dict[str, tuple[Feature, SimpleFeatureNecessity]]`

The return type is a dict with the name of the features as keys and the values are tuples of the feature and the necessity of the feature.
The `FeatureName`type is just a `str`. The `SimpleFeatureNecessity` class is an implementation of `FeatureNecessity` with the necessities `Optional` and `Required`.

:::tip
You can add new provided features by adding them to the returned dict.
:::

### _abstract_ _cls_ `requires_features`

Here the module can define which features are required by the module from the architecture. Mentioned `ExtensionFeatures` are passed to the module in the constructor.

**Example**

The [`ModuleStatusManager`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/modules/module_status_manager.html#ModuleStatusManager.requires_features) requires _CommunicationFeatures_ and `ExtensionFeatures`:
```python title="module_status_manager.py" showLineNumbers {5-9}
@classmethod
def requires_features(cls, config: ModuleConfig = ...) -> dict[FeatureName, tuple[Feature, FeatureNecessity]]:
    """Requires: ModuleStatusInfo, ModuleStates Outputs from other modules, ModuleManagerInstruction input. And the extensions: RunConfigAccess and the PySimpleGUIWindowExtension """
    return {
        ModuleStatusInfoComFeatureOut.name: (ModuleStatusInfoComFeatureOut, SimpleFeatureNecessity.Required),
        ModuleStatesComFeatureOut.name: (ModuleStatesComFeatureOut, SimpleFeatureNecessity.Required),
        ModuleManagerInstructionFeatureIn.name: (ModuleManagerInstructionFeatureIn, SimpleFeatureNecessity.Required),
        RunConfigAccessFeature.name: (RunConfigAccessFeature, SimpleFeatureNecessity.Required),
        PySimpleGUIWindowExtensionFeature.name: (PySimpleGUIWindowExtensionFeature, SimpleFeatureNecessity.Required),
    }
```

**Arguments**

| Name     | Type               | Details                                                                                      |
|----------|--------------------|----------------------------------------------------------------------------------------------|
| `config` | [`ModuleConfig`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/configuration/module_config.html#ModuleConfig) | the module config can provide information about the features to require in the architecture. |

**Return Value**

The different expected return types can have the following formats
- `dict[FeatureName, tuple[Feature, FeatureNecessity]]`
- `dict[str, tuple[Feature, FeatureNecessity]]`
- `dict[str, tuple[Feature, SimpleFeatureNecessity]]`

The return type is a dict with the name of the features as keys and the values are tuples of the feature and the necessity of the feature.
The `FeatureName`type is just a `str`. The `SimpleFeatureNecessity` class is an implementation of `FeatureNecessity` with the necessities `Optional` and `Required`.

:::tip
You can add new required features by adding them to the returned dict.
:::

### _cls_ `get_module_config_class`
Here a module can overwrite if it uses an extended version of the `ModuleConfig` as a config.
It is used to put the parsed config dict of the module in a class.

**Example**

```python title="module_status_manager.py" showLineNumbers {4}
@classmethod
def get_module_config_class(cls) -> Type[ModuleConfig]:
    """Use the ModuleStatusManagerConfig as a config."""
    return ModuleStatusManagerConfig
```

_No arguments_

**Return Value**

Returns the `ModuleConfig` class. Not an instance!

### _cls_ `get_start_function`

Here the module can specify a different start function that is called in the individual process. Here everything (in the process) starts. Overriding this method is usually not necessary.

**Default**
```python title="base.py" showLineNumbers {11}
@classmethod
def get_start_function(cls, config: ModuleConfig) -> Callable:
    """Provide the function that handles the start of the module (e.g., in a new process).

    Args:
        config: the module config if it depends on the config.

    Returns:
        the function reference.
    """
    return start_module
```

**Arguments**

| Name     | Type               | Details                                                                |
|----------|--------------------|------------------------------------------------------------------------|
| `config` | [`ModuleConfig`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/configuration/module_config.html#ModuleConfig) | the module config can provide information about how to run the module. |

**Return Value**

The arguments and key-word arguments of the returned function should be equivalent to the default function [`start_module`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/module/run.html#start_module).

---
title: Module Config
---

The base [`ModuleConfig`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/configuration/module_config.html#ModuleConfig) contains relevant attributes for the import and run phase of the module.

It can be extended by inheritance for specific modules with the `define(kw_only=True)` [attrs](https://www.attrs.org/en/stable/examples.html) decorator.

**Default**
```python title="module_config.py" showLineNumbers
@define
class ModuleConfig:
    module_path: str
    module_info: Type[ModuleInfo]
    expected_start_up_time: float | int  # to send initialized info to run time manager
    module_import_delay: float | int = 0  # not recommended, instead through communication receive info
    module_start_delay: float | int = 0  # not recommended, instead through communication receive info
    mean_frequency_step: float = 2
    restart_after_failure: bool = True
```

It is useful to set the `module_path` and `expected_start_up_time` in the derived version, for example, for the `ModuleStatusManager`
```python title="module_status_manager.py"
    module_path: str = "aaambos.std.modules.module_status_manager"
    ...
    expected_start_up_time: float | int = 0
```
and other attributes that you want to set or override.

The `module_info` needs to be named in the `arch_config.yml`, for example,
```yaml title="arch_config.yml" {3} showLineNumbers
modules:
  status_manager: 
    module_info: !name:aaambos.std.modules.module_status_manager.ModuleStatusManager
```
See [HyperPyYaml](/docs/hyperyml_configs) to learn what `!name:` and other HyperPyYaml concepts mean and how they work.

:::info
The name of the module is the key in the config, here `status_manager` (line 2).
:::
### Attributes set during architecture setup.
Some attributes are set during the architecture setup. They should not be set before that (in the config file).

```python title="module_config.py" showLineNumbers
    general_run_config: GeneralRunPart = None
    name: ModuleName = None
    ft_info: ModuleFeatureInfo | None = None
    com_info: ModuleCommunicationInfo | None = None
    module_extension_setups: dict[str, tuple[ExtensionSetup, ExtensionFeature]] | None = None
    logging: LoggingConfig | None = None
```

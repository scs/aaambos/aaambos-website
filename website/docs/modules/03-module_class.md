---
title: Module Class
---

The [`Module`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/module/base.html#Module) base class defines the methods that are called in the default `start_function`. Containing the `initialize`, `step`, `termination`, and `extension_step` methods.

:::tip
You can add better type hints for the communication service via the class attributes of a module:
```python
com: MsgTopicSendingAttribute | MsgPayloadWrapperCallbackReceivingAttribute | CommunicationService
```
:::

### _self_ `init`
Further, it sets relevant attributes in the `init` method. If the module requires additional extensions, these are added always to the `ext`ensions dict. If the extensions are passed additionally as a kwarg you might add it as a attribute to the module in a derived `init` method with a `super` call.

**Example**
```python title="module_status_manager.py" showLineNumbers {3-5}
def __init__(self, config: ModuleConfig, com: CommunicationService, log, ext: dict[str, Extension], run_config: RunConfig, gui_window: PySimpleGUIWindowExtension, *args, **kwargs):
    """Custom init, that sets also the run_config and gui_window extension"""
    super().__init__(config, com, log, ext, *args, **kwargs)
    self.run_config = run_config
    self.gui_window = gui_window
    self.steps = 0
    self.fst_states_received = False
    self.cur_log = None
    self.log_modified = None
    self.time_last_received = None
```

**Arguments**

| Name       | Type                                                                                                                                       | Details                                                                                                                           |
|------------|--------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|
| `config`   | [`ModuleConfig`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/configuration/module_config.html#ModuleConfig)         | the configuration of the module. The passed class depends on the `get_module_config_class` method in the assiciated `ModuleInfo`. |
| `com`      | [`CommunicationService`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/communication/service.html#CommunicationService) | the communication service instance. Which this the module can send msgs and can register callbacks for incoming messages.         |
| `log`      | [`LoguruLogger`](https://loguru.readthedocs.io/en/stable/api/logger.html#loguru._logger.Logger)                                            | a custom loguru logger for the module.                                                                                            |
| `ext`      | [`dict[str, Extension]`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/module/extension.html#Extension)               | a dict of the extensions that the module required / got enabled.                                                                  |
| `**kwargs` | `dict[str, Extensions]`                                                                                                                    | kwargs contain extensions that are requested and have the `as_kwarg` attribute enabled.                                           |

### _async self_ `initialize`
The method is called once. Here you can add callback registrations for the com, and other parts relevant for the initialization.

**Example**

The [`ModuleStatusManager`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/modules/module_status_manager.html#ModuleStatusManager.initialize) sets up class attributes, the GUI window (via the extension), and registers callbacks for communication promises.
```python title="module_status_manager.py" showLineNumbers {9-10}
async def initialize(self):
    """Set class attributes, set up the GUI, and register callback methods for communication promises. """
    self.module_statuses = defaultdict(list)
    self.module_states = defaultdict(list)
    self.gui_window.set_event_handler(self.handle_window_event)
    self.gui_window.window_closed_handler = self.window_closed_handler

    self.gui_window.setup_window(window_title="Module Status Manager", layout=self.create_layout(), theme=self.config.theme)
    await self.com.register_callback_for_promise(ModuleStatusInfoPromise, self.handle_module_state_info)
    await self.com.register_callback_for_promise(ModuleStatesPromise, self.handle_module_states)
```

_No arguments_

_No expected return value_

### _async self_ `step`
Iteratively called method. Optional: it can be an infinite generator method (`yield`). Introductions to generators: [Real Python](https://realpython.com/introduction-to-python-generators/), [programiz](https://www.programiz.com/python-programming/generator)

The module can implement in this method iterative code.

_No arguments_

_No expected return value_

### _self_ `terminate`
Is called during termination. Here should happen some clean up. Is not an _async_ method.

The `super` method calls the extension `terminate` method if they are present.

**Arguments**

At the moment, the `ControlMsg` argument does not contain that much information.

**Return Value**

Returns the exit code of the module/process. The standard `RunTimeManager` restarts modules after their unexpected termination if they return with an other value than `0`.

:::info
A module subclass will probably have further utility and callback methods for incoming msgs.
:::

### Start Function and Run Loop
The standard import, initialization, and run loop of a module is defined in the [`aaambos.core.module.run`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/module/run.html) file.


---
title: Vosk ASR
---

The [Vosk ASR](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_vosk_asr) module comes with the [Vosk](https://alphacephei.com/vosk/) speech recognition and a simulator which allows you to enter utterances via a text input.

## Installation

### Prerequisites
On Linux do
```bash
sudo apt-get install libportaudio2
```

### Lib
You can either clone the repo and do `pip install -e .`. Or you do:
```bash title="conda activate aaambos"
pip install basic_tts@git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_basic_tts@main
```

It should already download the models to `~/aaambos_agents/models/vosk/de`. If it did not happen, or you want to use different models, you need to download and extract them by hand.
[Download Link](https://alphacephei.com/vosk/models) for `vosk-model-small-de` and `vosk-model-spk`.

## Run
The simulator:
```yaml
modules:
  vosk_asr:
    module_info: !name:vosk_asr.modules.asr_simulator.AsrSimulator
    mean_frequency_step: 10
```
The Vosk ASR:
```yaml
modules:
  vosk_asr:
    module_info: !name:vosk_asr.modules.vosk_asr.VoskAsrModule
```
You can change the default models path (`"~/aaambos_agents/models/vosk/de/vosk-model-small-de-0.15"`) via the config name `vosk_model_path`.
And the device to receive the audio from via `device` which is passed to `sd.query_devices` as the first argument (At least on Ubuntu, it should be an integer - default is `0`).
```yaml
    vosk_model_path: "~/aaambos_agents/models/vosk/de/vosk-model-small-de-0.15"
    device: 0
```
The vosk aaambos pkg provides the [`vosk_microphone.py`](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_vosk_asr/-/blob/main/vosk_microphone.py?ref_type=heads) (copied from the documentation). You can list the microphone devices on your machine with `python vosk_microphone.py --list-devices`.
You can test/check out the speech recognition of different devices via `python vosk_microphone.py -m ~/aaambos_agents/models/vosk/de/vosk-model-small-de-0.15 -d 0`.

You can download other models [here](https://alphacephei.com/vosk/models).


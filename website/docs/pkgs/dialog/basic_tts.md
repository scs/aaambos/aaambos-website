---
title: Basic TTS
---

The [basic tts](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_basic_tts) uses [pyttsx3](https://github.com/nateshmbhat/pyttsx3) which uses the standard TTS on your system. On Ubuntu you need to install one.

:::note TTS
A TTS is a Text-To-Speech model. It creates from text a speech (audio) output and plays it directly via your speakers.
See [Wikipedia:Speech Synthesis](https://en.wikipedia.org/wiki/Speech_synthesis) for more background on that topic.
:::

## Installation

### Prerequisites
On Linux you need
```bash
sudo apt install espeak ffmpeg libespeak1
```

### Lib install

You can clone it and install it with `pip install -e .` or you do it with:
```bash title="conda acitvate aaambos"
pip install basic_tts@git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_basic_tts@main
```

## Running
You can add it to your modules in the `arch_config`:
```yaml title="arch_config.yml"
  basic_tts:
    module_info: !name:basic_tts.modules.basic_tts.BasicTts
```
The `language` and `rate` config keys are optional. The shown values are the default ones. The rate sets the words-per-minute rate of the TTS.
You can change the language by providing a prefix of the different languages of the pyttsx3 voices, e.g., `de` matches `de` and also `de_DE`.
```yaml
    language: de
    rate: 145
```

You can check out the voices by running:
```title="conda acitvate aaambos"
import pyttsx3
engine = pyttsx3.init()
for voice in engine.getProperty('voices'):
    for l in voice.languages:
        lang = l if isinstance(l, str) else l.decode('utf-8')
        print(lang[1:] if lang.startswith("\x05") else lang)
```

## Inside
It receives the [`SpeechGenerationControl`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_simple_flexdiam/simple_flexdiam/modules/core/definitions.html#SpeechGenerationControl) and sends [`SpeechGenerationStatus`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_simple_flexdiam/simple_flexdiam/modules/core/definitions.html#SpeechGenerationStatus) based on the execution status.
The messages and their payload are defined in the definitions in Simple Flexdiam. Therefore, the BasicTts requires the Simple Flexdiam pkg.
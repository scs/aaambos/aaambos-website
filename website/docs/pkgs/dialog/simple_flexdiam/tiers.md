---
title: Tiers
---

A tier stores event data and contains the methods that are called by the [communication service](/docs/communication#communication-service).

There are two types of events:
- **PointEvent** with one point in time. Incoming msgs are converted to PointEvents and their `time` attribute is used to set the point in time of the event. The data attribute contains the msg paylaod.
- **IntervalEvents** have a start and if ended/closed an end point.

A tier handles the communication features for sending and receiving msgs. Therefore, the `TierReq` needs the features passed.

New and updated events are send via the communications service if the event data corresponds to the payload wrapper classes of the communication promises. 
Further, the tier informs the observers that stated their `observe` interest.

:::info
In [observers](/docs/pkgs/dialog/simple_flexdiam/observers#write-own-observers), it is described how to add events to a tier via observers.
:::

## Define Tier

With the [`TierReq`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_simple_flexdiam/simple_flexdiam/modules/core/features.html#TierReq), you can define a tier. It is an _attrs_ class:
```python
@define(kw_only=True)
class TierReq:
    tier_name: str
    com_in_features: list[CommunicationFeature]
    com_out_features: list[CommunicationFeature]
    is_util: bool = False
```
For example, the `SpeechGenerationTier` is defined as follows:
```python
SPEECH_GENERATION = "SpeechGeneration"
SpeechGenerationTier = TierReq(tier_name=SPEECH_GENERATION, com_in_features=[SpeechGenerationStatusComFeatureIn], com_out_features=[SpeechGenerationControlComFeatureOut])
```
This tier receives/listen to the SpeechGeneration**Status** messages and sends/published new events that correspond to the SpeechGeneration**Control** wrapper. Have a look at the creation of communication promises and features automatically via [CLI](/docs/create#create-com_promise).

New defined `TierReq`s are mentioned in the `get_tiers` method of Observers. SimpleFlexdiam does the additional setup of the tiers automatically.

## Mention Tiers in an Observer
As an example, how to mention Tiers in the `get_tier` method, here the returned dict by the `SpeechGenerationObserver`:
```python title="SpeechGenerationObserver.get_tier"
{
    YieldTier: [ObservationType.WRITE, ObservationType.READ], 
    SpeechGenerationTier: [ObservationType.WRITE, ObservationType.OBSERVE], 
    FloorTier: ObservationType.READ, 
    PromptTier: ObservationType.WRITE
}
```

## The Timeboard
The timeboard contains all instantiated tiers. The setup and creation of the timeboard is done automatically. So no further setup from you.

It also passes all calls to the relevant tiers. Therefore, the observers just call the timeboard with the relevant references.
```python title="Inside the observer methods"
await self.timeboard.add_event(tier_name=SpeechGenerationTier.tier_name, event=event)
await self.timeboard.update_event(tier_name=SpeechGenerationTier.tier_name, event=event)
```

Retrieving (/reading) events from a tier can be done via the timeboard, too. At the moment, there is no utility method in the timeboard. 
Therefore, the observer need to access the tiers' dict. 
```python title="Inside the observer methods"
last_yield = self.timeboard.tiers[YieldTier.tier_name].get_last_event()
last_yield = self.timeboard.tiers[SpeechGenerationTier.tier_name].get_events_in_interval(start=datetime.now() + timedelta(seconds=10), end=datetime.now())
```


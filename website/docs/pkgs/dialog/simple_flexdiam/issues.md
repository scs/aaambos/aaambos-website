---
title: Issues
---

## Issue Class

Your custom issue class must implement the following methods:
```python
class MyIssue(Issue):
    @classmethod
    def get_init_shadow_children(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue] | tuple[Type[Issue], dict]]:
        pass

    @classmethod
    def potential_children_classes(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue]]:
        pass

    @classmethod
    def entry_points_of_interest(cls) -> list[str]:
        pass

    @classmethod
    def potential_xmit_classes(cls) -> list[Type[XMit]]:
        pass

    def handle_child_closed(self, child, context, output):
        pass

    def can_handle_entry_point(self, entry_point, ep_data, context) -> bool:
        pass

    def handle_entry_point(self, entry_point, ep_data, context: Context, output: IssueOutput):
        pass

    def handle_prompt_request(self, context, output):
        pass
```

### Init Shadow Issues
Return the issue classes that should be set as shadow issues. Optionally, with a config (see type annotation).

### Potential Children Classes
List all issue classes that the issue might have. (Not grandchildren, etc.).

### Entry Points of Interest
Mention all names of entry points the issue is/can be interested in.

### Potential XMIT classes
List all potential xmit classes that the issue might add to the output.

### Handle Child Close
Is called when a child is closed/terminated

### Can Handle Entry Point
Check if an entry point with its data can be handled.

### Handle Entry Point
Process the entry point with its data (is called if the can_handle_entry_point method returns True).

### Handle Prompt Request
Is called if the flow handler requests to issue to prompt (just iteratively when the agent could say something)

## Example Issue

Flexdiam comes with some issues. You can use them as examples. The cookiecutter template for Flexdiam addons comes with a further commented [example](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/cookiecutter-aaambos-pkg/-/blob/flexdiam_pkg/%7B%7Bcookiecutter.repo_name%7D%7D/%7B%7Bcookiecutter.__package_name%7D%7D/issues/example_issue.py?ref_type=heads).

````python
class ExampleIssue(Issue):
    """Handles NLU entry points "", "" by ... under the condition ... """
    __slots__ = ()  # important for proper cloning
    
    def initialize(self):
        # Setup part. Init of locals, etc. Here you CANNOT add xmits (e.g., speech output) or child issues.
        # Use the handle_prompt_request method instead. By checking for the first call. (create var in locals and update accordingly)
        pass

    @classmethod
    def get_init_shadow_children(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue] | tuple[Type[Issue], dict]]:
        # all issue classes from which the flow handler should create shadow children, when this issue is created.
        return []

    @classmethod
    def potential_children_classes(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue]]:
        # all classes that the issue could add as direct children. Including shadow issues.
        return []

    @classmethod
    def entry_points_of_interest(cls) -> list[str]:
        # add other entry points here if this issue needs to react/handle them.
        return [NLU_PARSE_ENTRY_POINT]

    @classmethod
    def potential_xmit_classes(cls) -> list[Type[XMit]]:
        # add other xmits here if this issue adds them to the output
        return [SpeechGenerationXMit]

    def handle_child_closed(self, child: Issue, context: Context, output: IssueOutput):
        # is called when a child issue reached an end state
        pass

    def can_handle_entry_point(self, entry_point: str, ep_data: Any, context) -> bool:
        # define when the issue can handle an entry point (e.g., by adding intentions, as strings, to the list)
        return entry_point == NLU_PARSE_ENTRY_POINT and ep_data.intention in []

    def handle_entry_point(self, entry_point: str, ep_data: Any, context: Context, output: IssueOutput):
        if self.can_handle_entry_point(entry_point, ep_data, context):  # should always be True

            # write attributes to store into locals (not directly into the issue)
            self.locals.greet_utterance = ep_data.utterance

            # use the utility method to add a speech output.
            self.say("Hello World!", floor_yield=5.0, output=output)

            # set the state of the issue to an end state (FULFILLED, FAILED)
            self.set_end_state(IssueState.FULFILLED)

            """ You can add also child issues to an issue.
            You should add their class in the list of the `potential_children_classes` method.
            ```
            output.add_child(MyOtherIssue, abstract_name="useful_abstract_child_issue_name", config={})
            ```
            If the child terminates, the handle_child_closed method is called.
            """

    def handle_prompt_request(self, context: Context, output: IssueOutput):
        # similar to the handle_entry_point method. Here you can add also issue execution logic.
        # This method is called on one active issue -> The flow handler request a prompt,
        # e.g., the agent could generate a speech output (floor is free)
        pass

    @staticmethod
    def say(text: str, floor_yield: float | int, output: IssueOutput) -> None:
        """Utility issue to add a speech output

        :param text: the utterance to generate
        :param floor_yield: the time to "yield"/block the floor afterward to allow the user to answer
        :param output: the issue output of the current execution (it is used to add the xmit)
        :return: None
        """

        speech_gen = SpeechGenerationControl(
            control=Controls.START,
            id=uuid.uuid4(),
            text=text,
        )
        xmit = SpeechGenerationXMit(speech_generation=speech_gen, floor_yield=timedelta(seconds=floor_yield))
        # similarly you can add other types of XMITs (== issue outputs)
        output.add_xmit(xmit)
````

If you want to require further observers via the issue, you can implement / overwrite the `further_observer_requirements` method:
```python
    @classmethod
    def further_observer_requirements(cls, issue_fh_config: IssueFHConfig) -> list[ObserverReq]:
        """Overwrite this function if the issue needs additional observers"""
        return []
```
You can specify additional aaambos architecture features in an issue similar to modules via the provides_features and requires_features methods:
```python
    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        """Overwrite if necessary"""
        return {}

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        """Overwrite if necessary"""
        return {}
```
:::caution
You cannot add attributes to an issue class! Use the locals attribute instead:
- `self.my_var = 123` does NOT work
- `self.locals.my_var = 123` does work
:::
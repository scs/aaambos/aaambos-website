---
title: Observers
---

## Default Observes

Several observers organize/manage the data on the different tiers. Flexdiam already implements some observers ([gitlab](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_simple_flexdiam/-/tree/main/simple_flexdiam/modules/std/observers?ref_type=heads)). These observers already handle the relevant parts necessary for simple dialogs. 

The **UserWordsObserver** observes the ASRTier which contains all ASR results. Based on that results the observer creates IntervalEvents that represent the user words on the UserWordsTier. Further, it updates the YieldTier to shorten floor yields if the user already answered.

The **NluEntryPointObserver** observes the NLUTier to trigger the `NLU_PARSE_ENTRY_POINT` on the floor handler. Currently, it ignores partial/hypotheses and only passes the final results.

The **SpeechGenerationObserver** receives SpeechGeneration XMits from the flow handler / or NLGObserver and puts them on the SpeechGenerationTier based on Yield- and FloorTier. So that it does not generate multiple utterances at once, does not interrupt the user, and does not touch the yielded floor.

The **AgentWordsObserver** creates IntervalEvents on the AgentWordsTier from the SpeechGenerationControl and -Status PointEvents.

The **FloorObserver** observes the UserWords- and AgentWordsTier and creates and updates the IntervalEvents on the FloorTier. These events are either 
- Silence
- User
- Agent or
- Overlap

The **NLGObserver** observes the NLGTier which gets NLGXMits from the flow handler (e.g., the issues). It generates the utterance the agent will say based on the information from the NLGRequest (part of the NLGXMit). It can directly generate it based on the type of NLGRequest (for example `Say` or `Fill` or could request an external module for generation). It outputs SpeechGenerationXMits to the corresponding observer. 

The GUI of Flexdiam is implemented as an observer like in the original Flexdiam. 
The **IssueGUI** is designed to visualize the tiers and issues from the IssueFlowHandler. 
Therefore, the IssueGUIObserver observers all tiers.

## Write Own Observers

For receiving and handle new input and [XMits](/docs/pkgs/dialog/simple_flexdiam/xmits) you need to write own Observer. Further, you can replace the observer above with own implementations or data flows.

The cookiecutter template for Flexdiam addons comes with a further commented [example](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/cookiecutter-aaambos-pkg/-/blob/flexdiam_pkg/%7B%7Bcookiecutter.repo_name%7D%7D/%7B%7Bcookiecutter.__package_name%7D%7D/observers/example_observer.py?ref_type=heads).

```python
class ExampleObserver(Observer):
    last_agent_words = None

    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def get_tiers(cls, config: SimpleFlexdiamConfig, observer_config: dict) -> dict[TierReq, ObservationType | list[ObservationType]]:
        # define here the tiers the observer wants to read, write, and observe (observe -> handle_new_event is called)
        # the tier already handles the com (In/Out) features
        return {}

    async def initialize(self):
        await super().initialize()

    async def handle_new_event(self, tier: Tier, event: Event):
        ...


# specify this in the `further_observer_requirements`-method of an issue that you use. (Or the flow handler directly)
exampl_observer_req = ObserverReq(oberserver_class=ExampleObserver, oberserver_config={})
```

Inside the `handle_new_event` method the observer can handle process the event differently:
- You can add new events to a tier (add write observation type to the get_tiers method):
```python
my_event = IntervalEvent(start_point=datetime.now(), data={}, short_view="shortInfoHereForGUI")  # end_point=datetime.now() + timedelta(seconds=10)
my_point_event = PointEvent(time_point=datetime.now(), data={}, short_view="shortInfoHereForGUI")
await self.timeboard.add_event(ExampleTier.tier_name, my_event)
await self.timeboard.add_event(OtherExampleTier.tier_name, my_point_event)
```
- You can update events:
```python
my_event.end_point = datetime.now() + timedelta(seconds=10)
await self.timeboard.update_event(ExampleTier.tier_name, my_event)
```
- You can call enter entry points to the flow handler by calling:
```python
await self.flow_handler.process_entry_point(NLU_PARSE_ENTRY_POINT, data, self.timeboard)
```
Change the entry point name and data to your needs.

If you want that your new observer handle a specific xmit class, the observer must implement the `add_xmit` method and the xmit class must reference/return the observer class in the `get_observer_class`-method. Have a look at the [SpeechGenerationXMit](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_simple_flexdiam/-/blob/main/simple_flexdiam/modules/std/xmits/speech_generation_xmit.py?ref_type=heads) and the [SpeechGenerationObserver](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_simple_flexdiam/-/blob/main/simple_flexdiam/modules/std/observers/speech_generation_observer.py?ref_type=heads).

## Observer and Tiers
Observer can observe, read, and write tiers. This means that they:
- are informed about new and updated events if they **observe** a tier,
- read events from a tier by hand if they **read** a tier,
- add or update events to a tier if they **write** on a tier.

## ObserverReq
Via the `ObserverReq` a flow handler sets the observers. Simple flexdiam does the initialization automatically. In the case of the IssueFlowHandler, issues can define/reference additional `ObserverReq`s via the `further_observer_requirements` method.

`ObserverReq` is a `attrs` class: 
```python
@define(kw_only=True)
class ObserverReq:
    oberserver_class: Type[Observer]
    oberserver_config: dict
```
An example is in the bigger code part above. The `further_observer_requirements` receives the `IssueFHConfig`. Therefore, you can configure the observer via configs by passing the relevant attributes to the `observer_config` dict.
You can specify complete additional observers via the `extra_observers` config key of the module config.
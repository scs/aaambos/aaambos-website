---
title: NLG
---

The Natural Language Generation (NLG) in **Simple Flexdiam** is managed through the `NLGObserver`. Using `NLGXMit`, you can pass an `NLGRequest` to the observer.
There are different types of `NLGRequest` available, two of which are already implemented: `Say` and `Fill`. These are immediate `NLGRequest` types that do not require further communication with other modules:
- **`Say`**: This allows you to specify exactly what should be spoken or written. For example: `Say("Hello World", ...)`.
- **`Fill`**: This leverages templates to introduce variability. You reference a template category via an identifier, e.g., `greeting`. A template is then selected based on the number of available slots that can be populated (using basic `.format` logic).

#### DelayedNLG Requests
Another NLG usage scenario is **DelayedNLG**, where messages are first sent, and results are then processed to generate the resulting utterance. Although the logic for this approach is implemented, a practical example of such a request has not been created yet. The relevant class for this functionality is `NLGDelayedProducer`.
#### Configuring Template (Fill) NLG
The configuration for template-based `Fill` NLG can be defined in the module configuration. Below is an example YAML configuration:
```yaml
  simple_flexdiam:
    module_info: !name:simple_flexdiam.modules.simple_flexdiam_module.SimpleFlexdiam
    observer_config:
      NLGObserver:
        templates: # you can also specify a file
          greeting:
            - "Hallo!"
            - "Hey!"
            - "Guten Tag!"
            - "Huhu!"
            - "Hallo {name}!"
            - "Hey {name}!"
```
#### Adding New `NLGRequest` Classes
To create a new type of `NLGRequest`, you need to define an `NLGProducer` (or a `DelayedNLGProducer` for delayed requests). Once you have created the new request type:
1. Register the `NLGRequest` class.
2. Link the producer class with the request type via `default_nlg_behavior_class`.

You can then use the new request class like this:
```python
NLGXmit(Say("Nice to meet you."))
```
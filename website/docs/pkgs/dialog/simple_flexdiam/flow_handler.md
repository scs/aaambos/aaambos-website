---
title: Flow Handler
---

The flow handler decides which output, e.g., Speech Generation, to generate. Further it receives incoming messages via entry points (Not all, because the observer already process some of these).

```mermaid
flowchart LR
    START:::hidden -- Entry Points -->    A[Flow Handler] -- XMits --> END:::hidden

    classDef hidden display: none;
```

## Issue Flow Handler
The issue flow handler organizes the dialog flow in [issues](/docs/pkgs/dialog/simple_flexdiam/issues). These are structured in an issue tree. Issues can have children. An active issue can process entry points and output xmits.
Shadow issues are issues that can be triggered with the correct entry point (and data).

The root issue starts with some shadow issues specified via the config.
```yaml title="arch_config.yml"
  simple_flexdiam:
    module_info: !name:simple_flexdiam.modules.simple_flexdiam_module.SimpleFlexdiam
    flow_handler:
      flow_handler_class: !name:simple_flexdiam.modules.issue_flow_handler.issue_flow_handler.IssueFlowHandler
      root_issue: !name:simple_flexdiam.modules.std.issues.root_issue.RootIssue
      shadow_root_issues:
        - !name:simple_flexdiam.modules.std.issues.greet_issue.GreetIssue
        - !name:simple_flexdiam.modules.std.issues.how_are_you_issue.HowAreYouIssue
        - !name:simple_flexdiam.modules.std.issues.repeat_issue.RepeatIssue
      entry_point_finder: !name:simple_flexdiam.modules.issue_flow_handler.simple_entry_point_finder.SimpleEntryPointFinder
      # entry_point_finder: !name:simple_flexdiam.modules.issue_flow_handler.selective_entry_point_finder.SelectiveEntryPointFinder
```

### Entry Point Finder

**Simple Entry Point Finder:**

The Simple EPF checks issues one after each other. So, if you add a shadow issue after the RepeatIssue it will never be called, because the RepeatIssue can handle all NLU_PARSE_ENTRY_POINT calls. 
Therefore, put your new issues above the RepeatIssue or remove the RepeatIssue from the config.

**Selective Entry Point Finder:**

The Selective EPF clones each issue that can handle an entry point and gathers all outputs. A scoring function selects the issues that should be realized. These cloned issues are replaced in the issue tree.
Important: The Issue class and all its child classes should only have the defined slots for proper cloning. Write your variables in the `locals` attribute instead of directly using the class dictionary. 
For correct IDE suggestions, your child classes of the Issue class should have `__slots__ = ()` defined.
You can reference the function that selects the issues in the `flow_handler` config via the `issue_output_comparison` key.
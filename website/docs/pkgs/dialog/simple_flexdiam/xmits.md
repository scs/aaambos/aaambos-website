---
title: XMits
---

XMits are the output class of issues. An issue can add (besides child issues) several XMits that should be realized. 

At the moment, XMits are very simple. In future versions, it might get more complex when simple flexdiam contains a more advanced issue planning.

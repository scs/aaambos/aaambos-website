---
title: Rasa NLU
---

The [Rasa NLU](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_rasa_nlu) is based on [Rasa](https://rasa.com/docs/rasa/). You can define your rasa pipeline and configure training data.
The task of the module is to extract an intent and possible entities of a transcribed utterance.

## Installation
You need to install some dependency by hand because of the weird quotation marks in the name of the library.
```bash title="conda activate aaambos"
pip install 'rasa[spacy]'
python3 -m spacy download de_core_news_sm
rasa telemetry disable
pip install nlu_rasa@git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_rasa_nlu@main
```
Optionally you can replace the last line with a local installation by cloning the repo and do `pip install -e .`

## Running
You can add it to the modules part of the `arch_config`:
```yaml title="arch_config.yml"
  rasa_nlu:
    module_info: !name:nlu_rasa.modules.rasa_nlu_module.RasaNluModule
    rasa_pipeline_config: _path_to_config_file_
    nlu_data_dir: _path_to_training_data_dir_
```

## Inside
For more details about the content look at the definitions in simple flexdiam of the [`NLUResults`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_simple_flexdiam/simple_flexdiam/modules/core/definitions.html#NluResults).
It might be interesting to look directly in the implementation: [module](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_rasa_nlu/nlu_rasa/modules/rasa_nlu_module.html) or NLUResults [example](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_rasa_nlu/nlu_rasa/modules.html).
It receives the [ASRResults](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_simple_flexdiam/simple_flexdiam/modules/core/definitions.html#AsrResultState) which trigger the NLU process.

:::info
Rasa needs to load the tensorflow model, etc. Therefore, the first ~20 seconds no utterances are parsed, but they are cached and parsed after the initialization.
:::

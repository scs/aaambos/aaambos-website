---
title: Extensions
---

## Message Cache Extension
The MessageCacheExtension can be used as a central unit to coordinate message/topic interests ([API Docis](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/extensions/message_cache.html)).
The last message of topics are cached and for new messages, callbacks can be registered. 
This is due to the definition in a class method (without access to the initialised class and its methods) and in a normal method.

```python title="Example Usage"
def __init__(self, msg_cache: MessageCacheExtension, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.msg_cache = msg_cache

# Inside the requires_features method
MessageCacheExtensionFeature.name: (MessageCacheExtensionFeature, SimpleFeatureNecessity.Required)
# You need still integrate the com In and Out features separately in the methods. This is a little bit unhandy. Also because the registration of interests happens during runtime.

# Later in an asynchronous method:
my_msg_interest = MessageInterest("MyMsgInterest", MyComPromise,
                                            required_features=[MyComOutFeature],
                                            provided_features=[MyComInFeature], callback=self.callback)
await msg_cache.register_interest(my_msg_interest)
# the callback method needs the same head/arguments as normal communication callbacks.
```
Obviously, it is easier to just register directly on the com service if you do not need the cache. You could implement a cache obviously by your self in the module directly. This extension was designed to be used with the Restricted Resource Extension where application can be added via the config and therefore the interest are not known beforehand. If you are interested, have a look at the [BehaviourScheduler](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_behaviour_scheduler) ([Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_behaviour_scheduler/behaviour_scheduler.html)).

## Module Status Extension
The ModuleStatusExtension is necessary for the ModuleStatusManager if it should show the module status (not state). With the extension, a module can set its current status which is then send to the manager ([API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/extensions/module_status/module_status_extension.html)).

```python title="Example Usage"
from aaambos.std.extensions.module_status.module_status_extension import ModuleStatusExtensionFeature, ModuleStatusInfoComFeatureOut, ModuleStatusExtension

def __init__(self, status: ModuleStatusExtension, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.status = status

# Inside the initialize or other asynchronous method:
await self.status.set_module_status("initialized", {"color": "blue"})
# change the arguments based on your needs or internal conventions

# inside the requires_features 
ModuleStatusExtensionFeature.name: (ModuleStatusExtensionFeature, SimpleFeatureNecessity.Required),
# Inside the provides_features
ModuleStatusInfoComFeatureOut.name: (ModuleStatusInfoComFeatureOut, SimpleFeatureNecessity.Required),
```

## Pipe Subprocess Extension
The PipeSubprocessExtension provides a module with the ability to start and manage a subprocess. It contains the logic to send data to the subprocess via pipes ([API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/extensions/pipe_subprocess/pipe_subprocess.html)).

```python title="Example Usage"
# Inside the __init__ method (via kwargs)
self.subprocess = subprocess

# Inside an asynchronous method (initialize):
await self.subprocess.start_subprocess(
    ["sh", "my_shell_script.sh"],
    callback=self.sim_callback,
    converter=self.convert_line
)

def convert_line(self, line):
    # msgs from the subprocess
    # obviously, depends on your subprocess and implementation
    line = json.loads(line)
    return line["topic"], line["msg"]

async def sim_callback(self, line):
    topic, msg = line  # output of convert_line

# In the same way you can send info to the subprocess
await self.subprocess.msg_subprocess({"topic": "my_topic", "msg": {"a": 1}})
# there you need to pass the msg

# Inside the requires_features method
PipeSubprocessExtensionFeature.name: (PipeSubprocessExtensionFeature, SimpleFeatureNecessity.Required),
```
In a python based subprocess (you maybe want to use a subprocess and python if you need to execute python code/libs in a different python version (change the conda environment in the shell script and start the python script)).
This is not the best way to do it, but it seems to work:
```python title="If your subprocess starts a python script"
# init
self.input_thread = Thread(target=self.receive_msgs)
self.input_thread.daemon = True
self.input_thread.start()

def receive_msgs(self):
    line_to_complete = ""
    for line in sys.stdin:
        try:
            line_to_complete += line
            msg = json.loads(line_to_complete)
            line_to_complete = ""
            assert "topic" in msg and "msg" in msg, "require topic and msg as keys in input string"
            self.msg_callback(msg["topic"], msg["msg"])
        except (KeyboardInterrupt, SystemExit):
            return
        except JSONDecodeError:
            print(line)
            pass  # wait for line to complete is parsable for multiline json
        except:
            sys.stderr.write(traceback.format_exc())
```

## Restricted Resource Extension
The RestrictedResourceExtension allows the definition of resources which can be requested by several references, which are called when the resource is free. It allows to request the resource with a priority ([API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/extensions/restricted_resouce.html)).

```python title="Example Usage"
RESOURCE_1, RESOURCE_2 = "resource1", "resource2"

# Inside __init__ from kwargs
self.resources = resources

# Inside initialize
self.resources.create_resource([RESOURCE_1, RESOURCE_2]) 

# inside the requires_features 
RestrictedResourceExtensionFeature.name: (RestrictedResourceExtensionFeature, SimpleFeatureNecessity.Required),
```
Different "things" that can acquire a resource are handled as "applicants" and they need to inherit from the `Applicant` class.
These applicants can then request resources:
```python title="Example Usage"
await resources.request_resource(RESOURCE_1, priority=2, applicant=self)
    # the handle_acquired_resource is called when the applicant acquires the resource

async def handle_acquired_resource(self, resource_name: ResourceIdentifier):
    ...

async def handle_resource_loss(self, resource_name: ResourceIdentifier):
    ...

# later
await resources.release_resource(RESOURCE_1, self)
```


## Run Config Access Extension
The RunConfigAccessExtension is NOT a child class of `Extension`. The passed object is the run_config parsed and adapted by the arch_setup. In general, the module only has access to the "general" part of the run_config which is part of the module config ([API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/extensions/run_config_access/run_config_access.html)). 

```python title="Example Usage"
def __init__(self, run_config: RunConfig, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.run_config = run_config

# Inside the requires_features method
RunConfigAccessFeature.name: (RunConfigAccessFeature, SimpleFeatureNecessity.Required),
```

---
title: GUIs
---

## PySimpleGUI

The PySimpleGUIWindowExtension allows to create a PySimpleGUI window by a module ([API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/guis/pysimplegui/pysimplegui_window_ext.html)).

```python
import PySimpleGUI as sg

from aaambos.std.guis.pysimplegui.pysimplegui_window_ext import PySimpleGUIWindowExtensionFeature
from aaambos.std.guis.pysimplegui.window_extension import PySimpleGUIWindowExtension

def __init__(self, config, gui_window: PySimpleGUIWindowExtension, *args, **kwargs):
    """Custom init, that sets also the run_config and gui_window extension"""
    super().__init__(config, *args, **kwargs)
    self.gui_window = gui_window    
    
# Inside the initialize method
self.gui_window.set_event_handler(self.handle_window_event)  # define own method
self.gui_window.setup_window(window_title="Module Status Manager", layout=self.create_layout())  # returns a list of lists as the layout. Look into PySimpleGUI.

def create_layout(self):
    return [[sg.Text("Text", key="my_txt")]]

async def handle_window_event(self, event, values):
    # event and values correspond to the PySimpleGUI returned objects from window.read()
    ...
    
# Later (in handle_window_event or step or other methods), access GUI objects with the keys defined in the creat_layout:
self.gui_window.window["my_txt"].update("New text")
    
# Inside the requires_features method:
PySimpleGUIWindowExtensionFeature.name: (PySimpleGUIWindowExtensionFeature, SimpleFeatureNecessity.Required),
```
The framerate/reaction time depends on the module step frequency. You can adapt it with the config parameter (Here set it to 24 hz):
```yaml
   mean_frequency_step: 24
```
Do not forget: This also increases the step frequency of your module!
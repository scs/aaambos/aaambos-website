---
title: Supervision
---

## Instruction Run Time Manager
You need it when you want to use the ModuleStatusManager. It has an own communication service which allows to insert instructions to the run time manager.

You can enable / use the InstructionRunTimeManager via the SupervisorConfig part (inside the run- and arch_config. Tested with run_config.)
```yaml
supervisor:
  run_time_manager_class: !name:aaambos.std.supervision.instruction_run_time_manager.instruction_run_time_manager.InstructionRunTimeManager
```

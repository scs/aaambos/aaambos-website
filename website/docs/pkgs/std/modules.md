---
title: Modules
---

## Module Status Manager
The ModuleStatusManager creates a GUI for displaying the module status, state, and logs. Further, it allows to instruct the InstructionRunTimeManager via buttons ([API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/modules/module_status_manager.html)).

```yaml
  status_manager:
    module_info: !name:aaambos.std.modules.module_status_manager.ModuleStatusManager
```

## Shell Command
The ShellCommandModule allows the execution of shell commands. The shell commands can be passed via the module config. How to activate a conda environment shows a shell script (that is called by the module) in the example folder of _aaambos_ ([API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/modules/shell_command.html)).

```yaml
shell_command_1:
  module_info: !name:aaambos.std.modules.shell_command.ShellCommandModule
  shell_command: ["sh", "~/PycharmProjects/aaambos/examples/conda_act.sh", "~/miniconda3/etc/profile.d/conda.sh", "base"]
```
Or any other command:
```yaml
  shell_command: ["more", "~/.bashrc"]
```
---
title: Features
---

Features are the core concept of the setup of the architecture. Each module states what features it _provides_ with a given necessity (required: need to be enabled; optional: can be enabled or disabled).
Further, each module states what features it _requires_ to be present and enabled in the rest of the architecture. 

An example: module _a1_ requires the features _f1_ (required) and _f2_ (optional) and provides feature _f3_ (optional). Further, module _a2_ provides feature _f1_. The module _a3_ requires feature _f3_ (required). It is a valid architecture, because the feature _f2_ is optionally required by _a1_ and therefore it does not need to be provided by another module. In a case of both parts be optional (required and provided), if this feature is enabled depends on the setup configuration (At the moment all features are turned on).

Different features exists, they are based on the base `Feature` class.

## Feature Attributes

The standard [`Feature`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/module/feature.html#Feature) has the following attributes.

| Name                  | Type                                                                                                                         | Details                                            |
|-----------------------|------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------|
| name                  | `str`                                                                                                                        | name of the feature, used to compare the feature   |
| version               | [`Version`](https://python-semanticversion.readthedocs.io/en/latest/reference.html#representing-a-version-the-version-class) | the version of the feature                         |
| requirements          | `list[Features]`                                                                                                             | other features that are required by the feature    |
| version_compatibility | [`SimpleSpec`](https://python-semanticversion.readthedocs.io/en/latest/reference.html#semantic_version.SimpleSpec)           | how this version is compatible with other versions |


## Feature Types

### `CommunicationFeature`
A [`CommunicationFeature`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/module/feature.html#CommunicationFeature) contains the required and provided communication promises. Maybe this is renamed later. To reduce the danger of confusion with the other level of required and provided features.

| Name            | Type                         | Details             |
|-----------------|------------------------------|---------------------|
| required_input  | `list[CommunicationPromise]` | the input promises  |
| provided_output | `list[CommunicationPromise]` | the output promises |

:::tip
The [`create_in_out_com_feature`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/std/communications/utils.html#create_in_out_com_feature) creates in and out features for a communication promise.

```python
MyNewMsgComFeatureIn, MyNewMsgComFeatureOut = create_in_out_com_feature(
    name=MY_NEW_MSG, promise=MyNewMsgPromise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0"
)
```
:::

### `ExtensionFeature`
An [`ExtensionFeature`](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/module/feature.html#ExtensionFeature) will be mentioned in the `requires_feaures` method, because it requires from the architecture to add the Extension.

| Name                   | Type   | Details                                                                                                                       |
|------------------------|--------|-------------------------------------------------------------------------------------------------------------------------------|
| as_kwarg               | `bool` | Should the extension be passed as a kwarg to the module constructor                                                           |
| kwarg_alias            | `str`  | The keyword if the extension is passed, else `None`                                                                           |
| shared_extension_setup | `bool` | Can the ExtensionSetup instance can be reduced for every Extension or should there be an instance for every Extension/Module. |

## Required and Provided Features

The features are mentioned in the `provides_features` and `requires_features` in the `ModuleInfo`.

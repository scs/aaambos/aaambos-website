---
title: The Run Config
---

## Role
The `RunConfig` should contain information about the specific agent that its run, like agent name.
Further, it should contain "secret" information, like keys, local paths, etc.
`RunConfig`s should make the `ArchConfig` as exchangeable as possible.

The `RunConfig` can overwrite module config attributes with the `module_options` key.

## Parts

See [API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/configuration/run_config.html#RunConfig)

### Module Options
You can also override module config attributes in the `run_config` via the `module_options` part.
You need to specify the module name and then the specific config attribute name with value.


:::tip Example
```yaml title=run_config.yml
module_options:
  sim_sub:
    conda_script: "~/miniconda3/etc/profile.d/conda.sh"
```
Where `sim_sub` is the name of the `QiBulletSim` module.
:::

### General Run Part
Defines general parts of the agent. Is copied to all module configs.
```yaml
general:
  agent_name: aaambos_test
  instance: _dev
  local_agent_directories: ~/aaambos_agents
  always_unique_instance: true
```

### Logging
Logging config, e.g., log level: `TRACE`, `DEBUG`, `INFO`, `SUCCESS`, `WARNING`, `ERROR`, `CRITICAL`.
```yaml
logging:
    log_level_command_line: TRACE
```

### Supervisor
[Supervisor config](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos/core/configuration/supervisor_config.html).
For example, the supervisor class or run_time_manager_class:
```yaml title="Using the InstructionRunTimeManager instead of the default one"
supervisor:
  run_time_manager_class: !name:aaambos.std.supervision.instruction_run_time_manager.instruction_run_time_manager.InstructionRunTimeManager
```

### Description
A natural language description. Just a str.

### RunConfigClass
You change the wrapper class of the RunConfig here.

### Settings
The RunConfigSettings are **set** by aaambos during runtime and are not intended to be part of the config file. 
It contains the feature and communication graph that are generated, the topic prefix based on the agent names, etc. and the instantiated communication setups.

## Future
- architecture: ArchitectureConvention
---
title: AAAMBOS Quickstart
---

## Configs
An architecture is defined in an `arch_config`.
For example, it could look like the following:

```yaml title="arch_config.yml" showLineNumbers
concurrency: MultiProcessing

communication:
  communication_prefs:
   - !name:ipaacar_com_service.communications.ipaacar_com.IPAACARInfo

modules:
  ping:
    module_info: !name:aaambos.std.modules.ping_pong_example.PingPongModule
    fst_send: 1
  pong:
    module_info: !name:aaambos.std.modules.ping_pong_example.PingPongModule
    fst_send: 0
  gui:
    module_info: !name:aaambos.std.guis.pysimplegui.pysimplegui_window.PySimpleGUIWindowModule
    window_title: Counting GUI
    topics_to_show: [["Counting", "aaambos.std.modules.ping_pong_example.CountingMsg", ["value"]]]
  status_manager:
    module_info: !name:aaambos.std.modules.module_status_manager.ModuleStatusManager
  shell_command_1:
    module_info: !name:aaambos.std.modules.shell_command.ShellCommandModule
    shell_command: ["sh", "~/PycharmProjects/aaambos/examples/conda_act.sh", "~/miniconda3/etc/profile.d/conda.sh", "label-studio"]
```
:::note
It contains five modules. Two instances of the `PingPongModule` named ping and pong. The config attribute `fst_send` is set.

Further, there is a GUI that shows the content of the value attribute of the payload from the messages on the `Counting` topic.

A status manager is started as a "Meta"-Module that will start a GUI that allows the administrator of the architecture to terminate and start modules.

The `ShellCommandModule` can execute shell commands.
:::

It uses [HyperPyYaml](/docs/hyperyml_configs) to reference Python objects and classes.

The `run_config` defines execution specific information like agent name, access keys, and passwords.
It can also override parts of the `arch_config`. An example of a `run_config` is:
```yaml title="run_config.yml" showLineNumbers
general:
  agent_name: aaambos_test
  instance: _dev
  local_agent_directories: ~/aaambos_agents

logging:
    log_level_command_line: WARNING

supervisor:
  run_time_manager_class: !name:aaambos.std.supervision.instruction_run_time_manager.instruction_run_time_manager.InstructionRunTimeManager
```
:::note
It sets the `agent_name`, `instance` name and the local directory where content of agents can be stored.

Further it sets the log level and the used RunTimeManager to [`InstructionRunTimeManager`](/docs/pkgs/std/supervision#instruction-run-time-manager) so that the adiministrator commands (e.g., from a GUI) can be converted to instructions to the RunTimeManager. 
:::

## Run an Agent/Architecture
To run an agent you need to specify the `run`- and `arch_config`. The following CLI command runs an agent:

```bash
aaambos run --run_config=my_package/configs/run_config.yml --arch_config=my_package/configs/arch_config_new.yml
```

You need to replace the path to your config files.

## Create New Parts of an Architecture
With AAAMBOS you can simplify the creation of pkgs, modules, extensions, etc. via the create command.
For example
```bash
aaambos create pkg
```
creates an AAAMBOS pkg. And
```bash
aaambos create module -n MyNewModule
```
creates a module python file with template code.

### Add to Config
If you created a module, either via a new pkg or just a new file, you need to add it to your architecture by adding it to your `arch_config`.
For example, if you created the MyNewModule module you need to add the following under the `modules` section in the config:
```yaml
name_of_my_module:
    module_info: !name:{{ cookiecutter.package_name }}.modules.my_new_module.MyNewModule
    my_config_attr: 1
```
where you can add further config attributes. But also replace the package name. You can name the module based on your needs (here `name_of_the_module`) -> Relevant if you want to have several instances of a module in one architecture.

Created pkgs need to be linked, so that they can be imported by python.
Run `pip install -e .` to do so.


---
title: Install AAAMBOS
slug: /
---

How to install _aaambos_ based on your plans with it:

- create and develop _aaambos_ pkgs (modules, extensions, etc.)
- run _aaambos_ agents
- develop the _aaambos_ core and std library

Further you probably need to install a communication service (in all three cases).

## Preparations

### Compatible OS
We recommend Ubuntu 22.04 or higher. Or similar Linux OS (see [manylinux_x_y compatibility](https://github.com/pypa/manylinux)).

### Conda environment with Python 3.10
We recommend using a conda environment with Python 3.10. Therefore, you need [miniconda](https://docs.conda.io/projects/miniconda/en/latest/) (recommended) or [Anaconda](https://docs.anaconda.com/free/anaconda/install/index.html) installed.
Create a conda environment with the name `aaambos`:
```bash
conda create -n aaambos python=3.10 -y
```
Activate the environment with
```bash
conda activate aaambos
```
:::tip
You can deactivate the conda environment with `conda deactivate`.

If you want to disable that all your terminals start in the base environment run the following command:
`conda config --set auto_activate_base false`

If you intall a lot of packages / use conda a lot, consider installing and using [mamba](https://mamba.readthedocs.io/en/latest/mamba-installation.html#mamba-install) instead of conda.
:::

### Use a capable IDE

Use an IDE like [PyCharm](https://www.jetbrains.com/pycharm/) to develop on _aaambos_ or _aaambos_ pkg. They provide useful tools that will detect errors and mistakes early on.
As a student you can also use the _Professional_ version instead of the free _Community_ edition.

:::info
If you want that _aaambos_ generates plots for your agents/architectures, you need to install graphviz dependencies:
```bash title="conda activate aaambos"
sudo apt-get install graphviz graphviz-dev
pip install pygraphviz
```
:::

## Installation for pkg creation and development

Install _aaambos_ with pip:
```bash
pip install aaambos@git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos@main
```
Similarly, install _aambos_ pkgs that you just want to use (import in other configs).

## Installation for just running agents

If you have an already defined agent/architecture (`run_` and `arch_config`) in an _aaambos_ pkg (and the pkg dependencies are up-to-date) you can just install the pkg and the _aaambos_ dependency will be installed: `pip install .` or  `pip install aaambos_pkg_xyz@...`.

## Installation for development on the core and std library

Similarly, you should/can install the aaambos pkgs you want to develop.

Clone the _aaambos_ repo in your development directory.
```bash
git clone https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos.git
```
Enter the directory.
```bash
cd aaambos
```

Install the repo locally in _editable_ mode:
```bash
pip install -e .
```

## Install a communication service

At the moment, you need to install the `ipaacar_com_serivce` if you want a communication service with all features (incremental/IU features).

If you do not need the incremental features and communication over a network you can also install the [`redis_com_service`](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_redis_com_service). 

You need to change the communication preferences in your `arch_config`s to your installed communication feature:
```yaml
communication:
  communication_prefs:
    # - !name:ipaacar_com_service.communications.ipaacar_com.IPAACARInfo
    - !name:redis_com_service.communications.redis_com.RedisServiceInfo
```


### IPAACAr communication service

The IPAACAr communication service is based on [IPAACAr](https://gitlab.ub.uni-bielefeld.de/scs/ipaacar), which provides wheels for [manylinux_x_y](https://github.com/pypa/manylinux).

For Python 3.10 and manylinux_2_34 the `aaambos_pkg_ipaacar_com_service` has dependencies defined.
You can install it via 
```bash
pip install ipaacar_com_service@git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_ipaacar_com_service.git
```

:::note
If that wheel is not supported on your platform/OS you need to build IPAACAr from source and install the _aaambos_ pkg without dependencies:

- Install Rust and Cargo using [rustup](https://rustup.rs/) (for linux/mac systems, otherwise download the .exe ([download](https://win.rustup.rs/x86_64))):
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

- Install [Maturin](https://www.maturin.rs/): 
```bash
pip install maturin
``` 
- Clone the IPAACAr git repository and change the directory to the `ipaacar-python` folder:
```bash
git clone https://gitlab.ub.uni-bielefeld.de/scs/ipaacar.git
cd ipaacar/ipaacar-python
```

- Build the wheel package inside the `ipaacar-python` folder: 
```bash 
maturin build --release
```
- Install the wheel (in the aaambos conda environment). Replace `FILENAME.whl` with the name of the created file:
```bash 
pip install ../target/wheels/FILENAME.whl
```
- Install `aaambos_pkg_ipaacar_com_service` without dependencies:
```bash
pip install ipaacar_com_service@git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_ipaacar_com_service.git --no-deps
```
:::

The string for your `arch_config.yml` in the communication preferences is:
```yaml
 - !name:ipaacar_com_service.communications.ipaacar_com.IPAACARInfo
```


### MQTT Broker
IPAACAr needs a running MQTT Broker. If you not already have one installed, do so:
NanoMQ and Mosquitto are such brokers. You can install [NanoMQ](https://nanomq.io/downloads?os=Linux) on Linux via:
1. Download the NanoMQ repository
```bash
curl -s https://assets.emqx.com/scripts/install-nanomq-deb.sh | sudo bash
```
2. Install NanoMQ
```bash
sudo apt-get install nanomq
```
3. Run NanoMQ
```bash
nanomq start
```
The started broker does block the terminal. You need to start it before you run an _aaambos_ agent but it does not need to restart for every agent.

For Windows installation see [here](https://nanomq.io/downloads?os=Windows). MacOS user might want to use MacPorts for installation (`sudo port install nanomq`) or search for a different broker (`brew install mosquitto`)

### Redis communication service
The Redis communication service is based on the [Redis Pub/Sub](https://redis.io/docs/interact/pubsub/) feature. In its current version, it is not able to handle IUs (incremental messages). Also, to my knowledge redis is not designed for communication over the network.
Until now, no pkg/module uses the IU feature (except the example PingPongIU module). So, the redis com service should work for you in most cases.

You need a running redis server on your machine. You can find the installation instruction for redis [here](https://redis.io/docs/install/install-redis/).
:::note
For Linux/Ubuntu you can do
```bash
curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list

sudo apt-get update
sudo apt-get install redis
```
You can test it by running:
```bash
redis-cli ping
```
It should print `PONG`.
:::
You can install the `redis_com_service` via:
```bash title="conda activate aaambos"
pip install redis_com_service@git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_redis_com_service@main
```
:::info
If you do not want to overwrite your local _aaambos_ installation, you need to add `--no-deps` to the `install` command and install `redis-py` by hand.
`pip install redis`.
:::

The string for your `arch_config.yml` in the communication preferences is:
```yaml
 - !name:redis_com_service.communications.redis_com.RedisServiceInfo
```

## Troubleshooting

:::caution
`ERROR: Could not build wheels for jsonnet-binary, which is required to install pyproject.toml-based projects`

**Solution**:
```bash
conda install -c conda-forge jsonnet
```
:::

:::caution
Error during installation of a custom-built **ipaacar** wheel `wheel not supported on this platform` on macOS ARM platform (M-Series).

Check what Python thinks is the running OS:
```pycon title="inside a python shell" 
>>> from distutils import util
>>> util.get_platform()
```
Example output: `macosx-10.9-x86_64‘` even if you are on an ARM platform. Adapt the name of the created wheel to your output. Install the wheel.
:::
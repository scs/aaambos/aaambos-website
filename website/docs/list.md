---
title: List
---

The `list` command gives you access to the history of agents that were executed via _aaambos_.
It will print out the date and time the execution started, the agent id, and the directory in which the data was stored for each agent.

```bash
aaambos list runs
```
will list all stored agent executions. Aliases are `history` and `agents`. _aaambos_ stores information about started agents in the `user_data_dir` (see [platformdirs](https://pypi.org/project/platformdirs/), on Linux it is `~/.local/share/aaambos`) in a CSV file.

In the future the command should list also other things, like installed pkgs, modules, extensions, etc.

:::note
An alias is `discover`. It can be used in the same way as `list`.
:::

Via the `list` command, you can also print out the content of the [global config](/docs/global_config):
```bash
aaambos list global_config
```
`aaambos_config` does the same.
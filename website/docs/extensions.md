---
title: Extensions
---

Extensions should encapsulate code/abilities that can be used in several modules.
So that only an instance of the extension should be created for a module instead of implementing all over again in the module.

The extensions are requested as a feature (`ExtensionFeature`) and are mentioned in the requires_features method.

Standard extensions are Hook, MessageCache, PipeSubprocess, and RestrictedResource. Extensions that are designed to need a specific module in the architecture but are set to other modules, is for example, the ModuleStatusExtension that contains the code for setting and sending the module status to the ModuleStatusManager module.

## Usage in a Module
Requiring an extension is just done if the `ExtensionFeature` is mentioned in the required features.

The extension instances are passed via the `ext`-kwarg dict of the init method of the module.
If the `ExtensionFeature` has set a positive `as_kwarg` attribute it is also passed as an individual kwarg with the (in the feature) specified kwarg name.

You can access the extension either via your the attribute (that you set in the init method) or via the `ext` attribute of the module:
```python
self.ext["MyExtension"]
```

### Methods
The `before_module_initialize` is called before the `initialize` method is called of the module. `after_module_initialize` is called afterwards. 


## Features

See [Features#ExtensionFeature](/docs/features#extensionfeature)

## Internals
Each `Extension` class references an `ExtensionSetup` class. It can also specify if the setup (!) class should be the same instance for all modules or should be a new one for every module.
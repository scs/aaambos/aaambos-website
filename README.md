![Build Status](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos-website/badges/main/pipeline.svg)

---

[A<sup>3</sup>MBOS](https://gitlab.ub.uni-bielefeld.de/scs/aaambos) website with [Docusaurus](https://docusaurus.io/) using GitLab Pages. [Website Link](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/)

Learn more about GitLab Pages at https://about.gitlab.com/features/pages and the official
documentation https://docs.gitlab.com/ee/user/project/pages/.

---

Important folders and files:
[Docs](website/docs), [Blog](website/blog), [Main Page](website/src/pages/index.js), [Docs Sidebar](website/sidebars.js), [General Settings](website/docusaurus.config.js)